import numpy as np
from solpspy.classes.mds import MdsData
from solpspy.classes.rundir import RundirData

from . import mdstt
from . import rundirtt

from solpspy.tools.tools import try_block, solps_property, module_path
from solpspy.tools.tools import priority
from . import masks

# =============================================================================
class MdsDataUSN(MdsData):
    """
    Mds+ class for Upper Single-Null configurations.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args,**kwargs)
        self._type = 'MdsDataUSN'
        self._magnetic_geometry = 'usn'

        self.target1 = mdstt.TargetData(self,'target1')
        self.target4 = mdstt.TargetData(self,'target4')

        self.out = self.target1
        self.inn = self.target4

        self.li = None
        self.ui = self.target4
        self.uo = self.target1
        self.lo = None

    @solps_property
    def iout(self):
        """int: Index of the main outer target (not guarding cell)."""
        self._iout = 1

    @solps_property
    def iinn(self):
        """int: Index of the main inner target (not guarding cell)."""
        self._iinn = self.nx-2

    @solps_property
    def ixp(self):
        """int: Index of the inner side of the x-point.
        First poloidal index inside SOL/Core.

        Notes
        -----
        Should it be changed to first in div?
        """
        self.log.warning("Untested.")
        self._ixp = self.rightcut[0]

    @solps_property
    def oxp(self):
        """int: Index of the outer side of the x-point.
        First poloidal index inside SOL/Core.

        Notes
        -----
        Should it be changed to first in div?
        """
        self.log.warning("Untested.")
        self._oxp = self.leftcut[0]+1


    @solps_property
    def masks(self):
        """Class containing different grid masks."""
        self._masks = masks.MasksUSN(self)



class RundirDataUSN(RundirData):
    """
    Rundir class for Upper Single-null configurations.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args,**kwargs)
        self._type = 'RundirDataUSN'
        self._magnetic_geometry = 'usn'

        self.target1 = rundirtt.TargetData(self,'target1')
        self.target4 = rundirtt.TargetData(self,'target4')

        self.out = self.target1
        self.inn = self.target4

        self.li = None
        self.ui = self.target4
        self.uo = self.target1
        self.lo = None

    @solps_property
    def iout(self):
        """int: Index of the main outer target (not guarding cell)."""
        self._iout = 1

    @solps_property
    def iinn(self):
        """int: Index of the main inner target (not guarding cell)."""
        self._iinn = self.nx-2

    @solps_property
    def ixp(self):
        """int: Index of the inner side of the x-point.
        First poloidal index inside SOL/Core.

        Notes
        -----
        Should it be changed to first in div?
        """
        self.log.warning("Untested.")
        self._ixp = self.rightcut[0]

    @solps_property
    def oxp(self):
        """int: Index of the outer side of the x-point.
        First poloidal index inside SOL/Core.

        Notes
        -----
        Should it be changed to first in div?
        """
        self.log.warning("Untested.")
        self._oxp = self.leftcut[0]+1


    @solps_property
    def masks(self):
        """Class containing different grid masks."""
        self._masks = masks.MasksUSN(self)

# =============================================================================








# =============================================================================
class MdsDataDDNU(MdsData):
    """
    Mds+ class for Disconnected Double Null Up configurations.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args,**kwargs)
        ##ATTENTION: This should be before super, but gives problems!
        self._type = 'MdsDataDDNU'
        self._magnetic_geometry = 'ddnu'

        self.target1 = mdstt.TargetData(self,'target1')
        self.target2 = mdstt.TargetData(self,'target2')
        self.target3 = mdstt.TargetData(self,'target3')
        self.target4 = mdstt.TargetData(self,'target4')

        self.inn2 = self.target1
        self.inn  = self.target2
        self.out  = self.target3
        self.out2 = self.target4

        self.li = self.target1
        self.ui = self.target2
        self.uo = self.target3
        self.lo = self.target3



    #Add all the cuts
    @solps_property
    def nncut(self):
        self._nncut = np.array([2])

    @solps_property
    def leftcut(self):
        self._leftcut = np.zeros(2,dtype=int)
        self._leftcut[0] = np.where(self.region[...,1]==2)[0][0]-1
        self._leftcut[1] = np.where(self.region[...,1]==3)[0][0]-1

    @solps_property
    def rightcut(self):
        self._rightcut = np.zeros(2,dtype=int)
        self._rightcut[0] = np.where(self.region[...,1]==7)[0][0]-1
        self._rightcut[1] = np.where(self.region[...,1]==6)[0][0]-1

    @solps_property
    def topcut(self):
        """
        The logic is somewhat hacked and might only be correct for a certain
        relation between radial flux surfaces in different regions.
        """
        self._topcut = np.zeros(2,dtype=int)

        tmpl = np.where(self.region[...,1]==2)[1][0]
        tmpr = np.where(self.region[...,1]==7)[1][0]
        if tmpl == tmpl:
            self._topcut[0] = tmpl-1

        self._topcut[1] = np.where(self.region[...,2]==4)[1][0]-1

    @solps_property
    def bottomcut(self):
        self._bottomcut = np.array([-1, -1])


    @solps_property
    def sep(self):
        """int: Index of the first flux surface in the inner SOL"""
        self._sep = np.min(self.topcut) + 1

    @solps_property
    def sep2(self):
        """int: Index of the first flux surface in the outer SOL"""
        self._sep2 = np.max(self.topcut) + 1


    @solps_property
    def iout(self):
        """int: Index of the main outer target (not guarding cell)."""
        self._iout = np.where(self.region[...,1] == 5)[0][0]

    @solps_property
    def iinn(self):
        """int: Index of the main inner target (not guarding cell)."""
        self._iinn = np.where(self.region[...,1] == 4)[0][0]-1


    @solps_property
    def iout2(self):
        """int: Index of the secondary outer target (not guarding cell)."""
        self._iout2 = np.where(self.region[...,1] == 8)[0][0]-1

    @solps_property
    def iinn2(self):
        """int: Index of the secondary inner target (not guarding cell)."""
        self._iinn2 = np.where(self.region[...,1] == 1)[0][0]

    @property
    def oxp(self):
        #""" Outer index of main x-point, first pol. cell inside divertor."""
        """ Outer index of main x-point,
        first pol. cell inside SOL/Core."""
        #return self.rightcut[1] #In div NO
        return self.rightcut[1]+1 #In SOL YES

    @property
    def oxp2(self):
        #""" Outer index of secondary x-point, first pol. cell inside divertor."""
        """ Outer index of secondary x-point,
        first pol. cell inside SOL/Core."""
        #return self.rightcut[0]+1 #In div NO
        return self.rightcut[0] #In SOL YES

    @property
    def ixp(self):
        #""" Inner index of main x-point, first pol. cell inside divertor."""
        """ Inner index of main x-point,
        first pol. cell inside SOL/Core."""
        #return self.leftcut[1]+1 #In div NO
        return self.leftcut[1] #In SOL YES

    @property
    def ixp2(self):
        #""" Inner index of secondary x-point, first pol. cell inside divertor."""
        """ Inner index of secondary x-point,
        first pol. cell inside SOL/Core."""
        #return self.leftcut[0] #In div NO
        return self.leftcut[0]+1 #In SOL YES

    @solps_property
    def masks(self):
        """Class containing different grid masks."""
        self._masks = masks.MasksDDNU(self)




class RundirDataDDNU(RundirData):
    """
    Rundir class for Disconnected Double Null Up configurations.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args,**kwargs)
        ##ATTENTION: This should be before super, but gives problems!
        self._type = 'RundirDataDDNU'
        self._magnetic_geometry = 'ddnu'

        self.target1 = rundirtt.TargetData(self,'target1')
        self.target2 = rundirtt.TargetData(self,'target2')
        self.target3 = rundirtt.TargetData(self,'target3')
        self.target4 = rundirtt.TargetData(self,'target4')

        self.inn2 = self.target1
        self.inn  = self.target2
        self.out  = self.target3
        self.out2 = self.target4

        self.li = self.target1
        self.ui = self.target2
        self.uo = self.target3
        self.lo = self.target3

        self._find_file('dstr')
        self._find_file('dstl')

    @solps_property
    def nncut(self):
        self._nncut = np.array([2])

    @solps_property
    def sep(self):
        """int: Index of the first flux surface in the inner SOL"""
        self._sep = np.min(self.topcut) + 1

    @solps_property
    def sep2(self):
        """int: Index of the first flux surface in the outer SOL"""
        self._sep2 = np.max(self.topcut) + 1

    @solps_property
    def iout(self):
        """int: Index of the main outer target (not guarding cell)."""
        self._iout = np.where(self.region[...,1] == 5)[0][0]

    @solps_property
    def iinn(self):
        """int: Index of the main inner target (not guarding cell)."""
        self._iinn = np.where(self.region[...,1] == 4)[0][0]-1


    @solps_property
    def iout2(self):
        """int: Index of the secondary outer target (not guarding cell)."""
        self._iout2 = np.where(self.region[...,1] == 8)[0][0]-1

    @solps_property
    def iinn2(self):
        """int: Index of the secondary inner target (not guarding cell)."""
        self._iinn2 = np.where(self.region[...,1] == 1)[0][0]

    @property
    def oxp(self):
        #""" Outer index of main x-point, first pol. cell inside divertor."""
        """ Outer index of main x-point,
        first pol. cell inside SOL/Core."""
        #return self.rightcut[1] #In div NO
        return self.rightcut[1]+1 #In SOL YES

    @property
    def oxp2(self):
        #""" Outer index of secondary x-point, first pol. cell inside divertor."""
        """ Outer index of secondary x-point,
        first pol. cell inside SOL/Core."""
        #return self.rightcut[0]+1 #In div NO
        return self.rightcut[0] #In SOL YES

    @property
    def ixp(self):
        #""" Inner index of main x-point, first pol. cell inside divertor."""
        """ Inner index of main x-point,
        first pol. cell inside SOL/Core."""
        #return self.leftcut[1]+1 #In div NO
        return self.leftcut[1] #In SOL YES

    @property
    def ixp2(self):
        #""" Inner index of secondary x-point, first pol. cell inside divertor."""
        """ Inner index of secondary x-point,
        first pol. cell inside SOL/Core."""
        #return self.leftcut[0] #In div NO
        return self.leftcut[0]+1 #In SOL YES


    #In test phase yet:
    @solps_property
    def masks(self):
        """Class containing different grid masks."""
        self._masks = masks.MasksDDNU(self)


    #ATTENTION: Improve docstrig
    #ATTENTION: Review logic
    def _extend(self, field):
        """
        Similar to b2plot/extend.F but only for grid dimensions.
        Should be similar to, but not implemented (why waste time right now?)
        For now, just use it for the standard case of connected spaces
        """
        # As for SN configurations
        ndim = list(field.shape)
        ndim[0], ndim[1] = ndim[0]+2, ndim[1]+2
        ndim = tuple(ndim)
        new_field = np.zeros(ndim)
        new_field[1:-1,1:-1] = field
        new_field[0] = new_field[1]
        new_field[-1] = new_field[-2]
        new_field[:,0] = new_field[:,1]
        new_field[:,-1] = new_field[:,-2]

        # Include 'internal' guarding cells too
        in1 = np.where(self.region[...,1] == 4)[0][0]
        in2 = np.where(self.region[...,1] == 5)[0][0]
        new_field[in1] = new_field[in1-1]
        new_field[in2-1] = new_field[in2]

        return new_field

# =============================================================================



# =============================================================================
class MdsDataDDN(MdsDataDDNU):
    """
    Mds+ class for Disconnected Double Null configurations.
    """


class RundirDataDDN(RundirDataDDNU):
    """
    Rundir class for Disconnected Double Null configurations
    """


# =============================================================================








##################### Connected Double-Null ###################################


# =============================================================================
class MdsDataCDN(MdsData):
    """
    Mds+ class for Connected Double Null configurations.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args,**kwargs)
        ##ATTENTION: This should be before super, but gives problems!
        self._type = 'MdsDataCDN'
        self._magnetic_geometry = 'cdn'

        self.target1 = mdstt.TargetData(self,'target1')
        self.target2 = mdstt.TargetData(self,'target2')
        self.target3 = mdstt.TargetData(self,'target3')
        self.target4 = mdstt.TargetData(self,'target4')

        self.inn  = self.target1
        self.inn2 = self.target2
        self.out2 = self.target3
        self.out  = self.target4

        self.li = self.target1
        self.ui = self.target2
        self.uo = self.target3
        self.lo = self.target3



    @solps_property
    def nncut(self):
        self._nncut = np.array([2])

    @solps_property
    def leftcut(self):
        """ Comparing outputs of rundirDDNU and CDNU, the way in which
        it is formed is the same.
        """
        self._leftcut = np.zeros(2,dtype=int)
        self._leftcut[0] = np.where(self.region[...,1]==2)[0][0]-1
        self._leftcut[1] = np.where(self.region[...,1]==3)[0][0]-1

    @solps_property
    def rightcut(self):
        """ Comparing outputs of rundirDDNU and CDNU, the way in which
        it is formed is the same.
        """
        self._rightcut = np.zeros(2,dtype=int)
        self._rightcut[0] = np.where(self.region[...,1]==7)[0][0]-1
        self._rightcut[1] = np.where(self.region[...,1]==6)[0][0]-1


    ## ATTENTION: THIS IS WRONG.
    @solps_property
    def topcut(self):
        """
        The logic is somewhat hacked and might only be correct for a certain
        relation between radial flux surfaces in different regions.
        THIS IS DDNU
        This must be wrong because it uses region[...,1]
        """
        #self._topcut = np.zeros(2,dtype=int)

        #tmpl = np.where(self.region[...,1]==2)[1][0]
        #tmpr = np.where(self.region[...,1]==7)[1][0]
        #if tmpl == tmpl:
        #    self._topcut[0] = tmpl-1

        #self._topcut[1] = np.where(self.region[...,2]==4)[1][0]-1

    @solps_property
    def bottomcut(self):
        """ Comparing outputs of rundirDDNU and CDNU, the way in which
        it is formed is the same.
        """
        self._bottomcut = np.array([-1, -1])


    @solps_property
    def sep(self):
        """int: Index of the first flux surface in the inner SOL"""
        self._sep = np.min(self.topcut) + 1


    @solps_property
    def iout(self):
        """int: Index of the main outer target (not guarding cell)."""
        #self._iout = np.where(self.region[...,1] == 8)[0][0]-1
        self._iout = np.where(self.region[...,1] == 8)[0][0]-1

    @solps_property
    def iinn(self):
        """int: Index of the main inner target (not guarding cell)."""
        self._iinn = np.where(self.region[...,1] == 1)[0][0]


    @solps_property
    def iout2(self):
        """int: Index of the secondary outer target (not guarding cell)."""
        self._iout2 = np.where(self.region[...,1] == 5)[0][0]

    @solps_property
    def iinn2(self):
        """int: Index of the secondary inner target (not guarding cell)."""
        self._iinn2 = np.where(self.region[...,1] == 4)[0][0]-1

    @property
    def oxp(self):
        """ Outer index of main x-point, first pol. cell inside divertor."""
        return self.rightcut[0]+1

    @property
    def oxp2(self):
        """ Outer index of secondary x-point, first pol. cell inside divertor."""
        return self.rightcut[1]

    @property
    def ixp(self):
        """ Inner index of main x-point, first pol. cell inside divertor."""
        return self.leftcut[0]

    @property
    def ixp2(self):
        """ Inner index of secondary x-point, first pol. cell inside divertor."""
        return self.leftcut[1]+1

    @solps_property
    def masks(self):
        """Class containing different grid masks."""
        self._masks = masks.MasksCDN(self)





class RundirDataCDN(RundirData):
    """
    Rundir class for Connected Double Null configurations.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args,**kwargs)
        ##ATTENTION: This should be before super, but gives problems!
        self._type = 'RundirDataCDN'
        self._magnetic_geometry = 'cdn'

        self.target1 = rundirtt.TargetData(self,'target1')
        self.target2 = rundirtt.TargetData(self,'target2')
        self.target3 = rundirtt.TargetData(self,'target3')
        self.target4 = rundirtt.TargetData(self,'target4')

        self.inn  = self.target1
        self.inn2 = self.target2
        self.out2 = self.target3
        self.out  = self.target4

        self.li = self.target1
        self.ui = self.target2
        self.uo = self.target3
        self.lo = self.target3

        #Are these names correct for CDN?
        self._find_file('dstr')
        self._find_file('dstl')

    @solps_property
    def nncut(self):
        self._nncut = np.array([2])

    @solps_property
    def sep(self):
        """int: Index of the first flux surface in the inner SOL"""
        self._sep = np.min(self.topcut) + 1


    @solps_property
    def iout(self):
        """int: Index of the main outer target (not guarding cell)."""
        self._iout = np.where(self.region[...,1] == 8)[0][0]-1

    @solps_property
    def iinn(self):
        """int: Index of the main inner target (not guarding cell)."""
        self._iinn = np.where(self.region[...,1] == 1)[0][0]


    @solps_property
    def iout2(self):
        """int: Index of the secondary outer target (not guarding cell)."""
        self._iout2 = np.where(self.region[...,1] == 5)[0][0]

    @solps_property
    def iinn2(self):
        """int: Index of the secondary inner target (not guarding cell)."""
        self._iinn2 = np.where(self.region[...,1] == 4)[0][0]-1

    @property
    def oxp(self):
        """ Outer index of main x-point, first pol. cell inside divertor."""
        return self.rightcut[0]+1

    @property
    def oxp2(self):
        """ Outer index of secondary x-point, first pol. cell inside divertor."""
        return self.rightcut[1]

    @property
    def ixp(self):
        """ Inner index of main x-point, first pol. cell inside divertor."""
        return self.leftcut[0]

    @property
    def ixp2(self):
        """ Inner index of secondary x-point, first pol. cell inside divertor."""
        return self.leftcut[1]+1

    @solps_property
    def masks(self):
        """Class containing different grid masks."""
        self._masks = masks.MasksCDN(self)

    ##In test phase yet:
    #@solps_property
    #def masks(self):
    #    """Class containing different grid masks."""
    #    self._masks = MasksDDNU(self)


    ##ATTENTION: Improve docstrig
    ##ATTENTION: Review logic
    #def _extend(self, field):
    #    """
    #    Similar to b2plot/extend.F but only for grid dimensions.
    #    Should be similar to, but not implemented (why waste time right now?)
    #    For now, just use it for the standard case of connected spaces
    #    """
    #    # As for SN configurations
    #    ndim = list(field.shape)
    #    ndim[0], ndim[1] = ndim[0]+2, ndim[1]+2
    #    ndim = tuple(ndim)
    #    new_field = np.zeros(ndim)
    #    new_field[1:-1,1:-1] = field
    #    new_field[0] = new_field[1]
    #    new_field[-1] = new_field[-2]
    #    new_field[:,0] = new_field[:,1]
    #    new_field[:,-1] = new_field[:,-2]

    #    # Include 'internal' guarding cells too
    #    in1 = np.where(self.region[...,1] == 4)[0][0]
    #    in2 = np.where(self.region[...,1] == 5)[0][0]
    #    new_field[in1] = new_field[in1-1]
    #    new_field[in2-1] = new_field[in2]

    #    return new_field

# =============================================================================



# =============================================================================




##ATTENTION: Improve docstring in all of the class
##ATTENTION:
##Found an error about how masks are implemented. If outdiv_nolim is used
##before masks.outdiv, the result is different because the guarding cell
##is either accepted or not. That is wrong and has to be fixed.
#class MasksCDN(object):
#    """Class containing different grid masks."""
#
#    def __init__(self,mother):
#        self._mother = mother
#        self.log = self._mother.log
#        self.irlcl = True
#
#    ## ATTENTION!
#    @solps_property
#    def gdcl(self):
#        """(nx,ny): Guard cells."""
#        self._gdcl = np.zeros((self._mother.nx,self._mother.ny))
#        ## WRONG???
#        #self._gdcl[0,:] = 1
#        #self._gdcl[:,0] = 1
#        #self._gdcl[self._mother.nx-1,:] = 1
#        #self._gdcl[:,self._mother.ny-1] = 1
#
#        self._gdcl[0] = 1
#        self._gdcl[-1] = 1
#        self._gdcl[:,0] = 1
#        self._gdcl[:,-1] = 1
#        self._gdcl[self._mother.iout-1,:] = 1
#        self._gdcl[self._mother.iinn+1,:] = 1
#
#    @property
#    def guard_cells(self):
#        """Alias for self.gdcl"""
#        return self.gdcl
#
#    @solps_property
#    def rlcl(self):
#        """(nx,ny): Real cells."""
#        #self._rlcl = np.ones((self._mother.nx,self._mother.ny)) - self.gdcl
#        self._rlcl = np.logical_not(self.gdcl)
#
#    @property
#    def real_cells(self):
#        """Alias for self.rlcl"""
#        return self.rlcl
#
#    @solps_property
#    def lfs(self):
#        """ Low field side region of grid.
#        For DDN is quite simple, as it is naturally partitioned.
#        Outmainsol are missing.
#        """
#        self._lfs = self.outdiv + self.outdiv2 + self.outcore + self.outmainsol + self.outmainsol2
#
#
#    def _regions(self,regions, mregions = None, rlcl=True):
#        """
#        regions: dict
#        """
#        str_to_reg = {'v':0, 'x':1, 'y':2}
#
#        # 'AND' regions
#        mask = np.ones((self._mother.nx, self._mother.ny))
#        for d, inds in list(regions.items()):
#            if isinstance(inds,int) or isinstance(inds,float):
#                inds = [int(inds)]
#            if isinstance(d,str):
#                d = str_to_reg[d]
#            for ind in inds:
#                tmp = self._mother.region[...,d] == ind
#                mask = np.logical_and(mask, tmp)
#
#        if rlcl:
#            return np.logical_and(mask,self.rlcl)
#        else:
#            return mask
#
#
#    ## Region definitions
#    ## ATTENTION: Some like inndiv are wrong in CDN.
#    @solps_property
#    def outdiv(self):
#        """ (nx,ny): Main outer divertor region."""
#        self._outdiv = self._regions({'v':7}, rlcl=self.irlcl)
#
#    @solps_property
#    def inndiv(self):
#        """ (nx,ny): Main inner divertor region."""
#        self._inndiv = self._regions({'v':4}, rlcl=self.irlcl)
#
#    @solps_property
#    def outdiv2(self):
#        """ (nx,ny): Main outer divertor region."""
#        self._outdiv2 = self._regions({'v':8}, rlcl=self.irlcl)
#
#    @solps_property
#    def inndiv2(self):
#        """ (nx,ny): Main inner divertor region."""
#        self._inndiv2 = self._regions({'v':3}, rlcl=self.irlcl)
#
#    @solps_property
#    def outsol(self):
#        """ (nx,ny): Outer main chamber SOL region."""
#        self._outsol = self._regions({'v':6}, rlcl=self.irlcl)
#
#    @solps_property
#    def innsol(self):
#        """ (nx,ny): Inner main chamber SOL region."""
#        self._innsol = self._regions({'v':2}, rlcl=self.irlcl)
#
#
#    @solps_property
#    def outcore(self):
#        """ (nx,ny): Outer core region."""
#        self._outcore = self._regions({'v':5}, rlcl=self.irlcl)
#
#    @solps_property
#    def inncore(self):
#        """ (nx,ny): Inner core region."""
#        self._inncore = self._regions({'v':1}, rlcl=self.irlcl)
#
#
#
#
#
#
#
#
#
#
#
#    @solps_property
#    def core(self):
#        self._core = np.logical_and(self.outcore, self.inncore)
#
#    @solps_property
#    def notcore(self):
#        self._notcore = self.rlcl*(1-self.core)
#
#
#    @solps_property
#    def pfr(self):
#        self._pfr = (1-self.sol) * self.notcore * self.rlcl
#
#
#
#    @solps_property
#    def div(self):
#        """ Main Inner + Outer divertors.
#        Guard cells are excluded.
#        """
#        self._div = self.inndiv + self.outdiv
#    @property
#    def divertor(self):
#        """Alias of self.div"""
#        return self.div
#
#    @solps_property
#    def notdiv(self):
#        """ Everything except inner+outer divertors.
#        Guard cells are excluded.
#        """
#        self._notdiv = self.rlcl*(np.ones((self._mother.nx,self._mother.ny))-self.div)
#
#
#    @solps_property
#    def div2(self):
#        """ Secondary Inner + Outer divertors.
#        Guard cells are excluded.
#        """
#        self._div2 = self.inndiv2 + self.outdiv2
#    @property
#    def divertor2(self):
#        """Alias of self.div"""
#        return self.div2
#
#    @solps_property
#    def notdiv2(self):
#        """ Everything except inner+outer divertors.
#        Guard cells are excluded.
#        """
#        self._notdiv2 = self.rlcl*(np.ones((self._mother.nx,self._mother.ny))-self.div2)
#
#
#    # Not valid without region definitions but this is ok anyway.
#    @solps_property
#    def innertarget(self):
#        self._innertarget = np.zeros((self._mother.nx,self._mother.ny))
#        self._innertarget[self._mother.iinn,:] = 1 # nx=1 (rlcl)
#
#    @solps_property
#    def outertarget(self):
#        self._outertarget = np.zeros((self._mother.nx,self._mother.ny))
#        self._outertarget[self._mother.iout,:] = 1
#
#    @solps_property
#    def innertarget2(self):
#        self._innertarget2 = np.zeros((self._mother.nx,self._mother.ny))
#        self._innertarget2[self._mother.iinn2,:] = 1 # nx=1 (rlcl)
#
#    @solps_property
#    def outertarget2(self):
#        self._outertarget2 = np.zeros((self._mother.nx,self._mother.ny))
#        self._outertarget2[self._mother.iout2,:] = 1



















