import os
import sys
import numpy as np
import pytest
from solpspy import *
from solpspy.tools.tools import module_path, yaml_loader, SimpleNamespace

def runs_description():
    #return yaml_loader(os.path.join(module_path(),'tests/runs_description.yaml'))
    return yaml_loader(os.path.join(module_path(),'../tests/runs_description.yaml'))


## An easier way to do skip when MDSplus is not available would be to
## Generate the shot number in a different way and construct both
## MDS and Rundir as Rundir is constructed at the moment.
## So that MDS can have importorskip("MDSplus")


@pytest.fixture(scope="session", params=[94908, 119603])
def nshot(request):
    #dummy = SimpleNamespace()
    #dummy.nshot = request.param
    return request.param


@pytest.fixture(scope="session")
def mds(nshot):
    pytest.importorskip("MDSplus")
    valid_momentum_balance =['MdsData', 'RundirData']
    dummy = SolpsData(nshot)
    if dummy._type in valid_momentum_balance:
        dummy.momentum_balance(printing = False) #To be changed
    return dummy


## PREVIOUS IDEA
#@pytest.fixture(scope="session", params=[94908, 119603])
#def mds(request):
#    valid_momentum_balance =['MdsData', 'RundirData']
#    try:
#        dummy = SolpsData(request.param)
#        if dummy._type in valid_momentum_balance:
#            dummy.momentum_balance(printing = False) #To be changed
#    except: #In case that MDSplus is not available
#        dummy = SimpleNamespace()
#        dummy.nshot = request.param
#    return dummy



@pytest.fixture(scope="session")
#@pytest.fixture(scope="module")
def rundir(nshot):
    valid_momentum_balance =['MdsData', 'RundirData']
    #path = os.path.join(module_path(),
    #              'tests/rundir_src/'+str(nshot)+'/run')
    path = os.path.join(module_path(),
                  '../tests/rundir_src/'+str(nshot)+'/run')
    dummy = SolpsData(path)
    if dummy._type in valid_momentum_balance:
        dummy.momentum_balance(printing = False) #To be changed
    return dummy












#Use global mds/rundir for now. If not possible, resort back to these and
#separated tests.

#@pytest.fixture(scope="module", params=[94908])
#def mds_sn(request):
#    dummy = MdsData(request.param)
#    dummy.momentum_balance(printing = False) #To be changed
#    return dummy
#
#@pytest.fixture(scope="module")
#def rundir_sn(mds_sn):
#    dir = os.path.join(module_path(),
#                  'tests/rundir_src/'+str(mds_sn.nshot)+'/run')
#    dummy = RundirData(dir)
#    dummy.momentum_balance(printing = False) #To be changed
#    return dummy
#
#
#
#
#
#@pytest.fixture(scope="module", params=[119603])
#def mds_ddn(request):
#    dummy = MdsDataDDNU(request.param)
#    return dummy
#
#@pytest.fixture(scope="module")
#def rundir_ddn(mds_ddn):
#    dir = os.path.join(module_path(),
#                  'tests/rundir_src/'+str(mds_ddn.nshot)+'/run')
#    dummy = RundirDataDDNU(request.param)
#    return dummy






#--------------------- Validation tools -----------------------------------
def validity_crosscheck(mds, rundir, var, var2=None):
    try:
        if var2 is None:
            var2 = var
        mdsvar = getattr(mds,var)
        runvar = getattr(rundir,var2)
    except:
        try:
            var = var.split('.')
            if var2 is None:
                var2 = var
            else:
                var2 = var2.split('.')
            mdsvar = getattr(getattr(mds,var[0]),var[1])
            runvar = getattr(getattr(rundir,var2[0]),var2[1])
        except:
            return False
    try:
        return np.allclose(mdsvar,runvar)
    except:
        try:
            return (mdsvar == runvar).all()
        except:
            try:
                return (mdsvar == runvar)
            except:
                return False


def validity_mds(mds,var):
    #mds_var_npy = os.path.join(module_path(),
    #              'tests/mds_npy/'+str(mds.nshot)+'/'+var+'.npy')
    mds_var_npy = os.path.join(module_path(),
                  '../tests/mds_npy/'+str(mds.nshot)+'/'+var+'.npy')
    dummy = np.load(mds_var_npy)
    try:
        numvar = getattr(mds,var)
    except:
        try:
            var = var.split('.')
            numvar = getattr(getattr(mds,var[0]),var[1])
        except:
            return False

    if numvar is None:
        try:
            return (dummy == np.array(None))
        except:
            return False
    else:
        try:
            return np.allclose(numvar,dummy)
        except:
            try:
                return (numvar == dummy).all()
            except:
                try:
                    return (numvar == dummy.astype(str))
                except:
                    return False


#def validity_rundir(rundir, var, var2=None):
#    #import pdb; pdb.set_trace()
#    npy_path = os.path.join(module_path(),
#            'tests/rundir_npy/'+str(rundir.shot))
#    npyvar=None
#    try:
#        if var2 is None:
#            var2 = var
#        runvar = getattr(rundir,var)
#        try:
#            npyvar = np.load(os.path.join(npy_path,var2+'.npy'))
#        except:
#            import pdb; pdb.set_trace()
#            # In case the npy file does not exist
#            pytest.skip(var2+'.npy could not be read!')
#
#    except:
#        try:
#            var = var.split('.')
#            if var2 is None:
#                var2 = var
#            else:
#                var2 = var2.split('.')
#            runvar = getattr(getattr(rundir,var[0]),var[1])
#            try:
#                npyvar = np.load(os.path.join(npy_path,
#                    var2[0]+'.'+var2[1]+'.npy'))
#            except: # In case the npy file does not exist
#                pytest.skip(
#                    var2[0]+'.'+var2[1]+'.npy could not be read!')
#        except:
#            return False
#    if npyvar is None:
#        pytest.skip('npy file could not be read!')
#
#    try:
#        return np.allclose(runvar,npyvar)
#    except:
#        try:
#            return (npyvar == runvar).all()
#        except:
#            try:
#                return (npyvar == runvar)
#            except:
#                return False


def validity_rundir(rundir, var, var2=None):
   # import pdb; pdb.set_trace()
    #npy_path = os.path.join(module_path(),
    #        'tests/rundir_npy/'+str(rundir.shot))
    npy_path = os.path.join(module_path(),
            '../tests/rundir_npy/'+str(rundir.shot))
    try:
        if var2 is None:
            var2 = var
        runvar = getattr(rundir,var)
        npyvar = np.load(os.path.join(npy_path,var2+'.npy'))

    except:
        try:
            var = var.split('.')
            if var2 is None:
                var2 = var
            else:
                var2 = var2.split('.')
            runvar = getattr(getattr(rundir,var[0]),var[1])
            npyvar = np.load(os.path.join(npy_path,
                    var2[0]+'.'+var2[1]+'.npy'))
        except:
            pytest.skip('npy file could not be read!')
            return False

    try:
        return np.allclose(runvar,npyvar)
    except:
        try:
            return (npyvar == runvar).all()
        except:
            try:
                return (npyvar == runvar)
            except:
                return False


#--------------------- Skipping   tools -----------------------------------
def skip_by_species(sim, ns):
    if sim.ns < ns:
        pytest.skip('Number of fluid species is just {}.'.format(str(sim.ns)))

def skip_by_especies(sim, natm):
    if sim.natm < natm:
        pytest.skip('Number of Eirene species is just {}.'.format(str(sim.natm)))

def skip_b2standalone(sim):
    if sim.natm is None and sim.nmol is None and sim.nion is None:
        pytest.skip('B2standalone simulation.')


def skip_ddn(sim):
    ddn_types =['MdsDataDDNU', 'RundirDataDDNU']
    if sim._type in ddn_types:
        pytest.skip('Test not for DDN simulations.')

def skip_sn(sim):
    ddn_types =['MdsDataDDNU', 'RundirDataDDNU']
    if sim._type not in ddn_types:
        pytest.skip('Test only for DDN features.')


def skip_no_mdsplus_module():
    try:
        import MDSplus

        #ATTENTION 2021:
        #Monkey patch to allow for MDSplus disabling??
        assert sys.modules['MDSplus'] != 0

    except:
        pytest.skip('Test only for RundirData like objects.')




