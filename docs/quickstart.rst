Quickstart
==========

For installagion, c.f.: :ref:`installation`


With hide-code:

.. jupyter-execute::
    :hide-code:

    import numpy as np

    tmp = [
        {'param1':['p1v1', 'p1v2'], 'param2':['p2v1', 'p2v2']},
        {'param1':['p1v3', 'p1v4'], 'param3':['p3v1', 'p3v2'], 'param4':'p4v1'},
        ] 
    print(tmp)


Without hide-code:

.. jupyter-execute::

    import numpy as np

    tmp = [
        {'param1':['p1v1', 'p1v2'], 'param2':['p2v1', 'p2v2']},
        {'param1':['p1v3', 'p1v4'], 'param3':['p3v1', 'p3v2'], 'param4':'p4v1'},
        ] 
    print(tmp)
