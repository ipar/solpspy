from __future__ import division, print_function

import matplotlib.pyplot as plt

from solpspy.tools.tools import priority



def balance(quant, level=2, **kwargs):
    prettify = priority('prettify', kwargs, {})

    direction = priority(['dir','direction'], kwargs, 'radial')

    #Fig
    subplots = priority('subplots', prettify, kwargs, True)




def radial_profiles(mother, **kwargs):
    """
    Mother contains the variables to be plotted
    """




    xlim = np.max(rmrsep[rind])
    dict_print = {
            'subplots': True,
            'xlim_1':(-xlim, xlim),
            'ylim_1':(None, None),
            'xlim_2':(-xlim, xlim),
            'ylim_2':(None, None),
            'ylabel_1_extra':'',
            'ylabel_2_extra':'',
            'legend_font_size':12,
            'save':False,
            'save_path':'.',
            'save_name_1':'pressure_profiles.pdf',
            'save_name_2':'details_profiles.pdf'}
    if dict_print['subplots']:
           dict_print['figsize'] = (16,13.6)
    #else: 
    #       dict_print['figsize'] = (8,6)

    for key in dict_print: 
        try:
            dict_print[key] = dictp[key]
        except:
            pass

    if dict_print['subplots']:
        try:
            fig = plt.figure(figsize=dict_print['figsize'])
        except:
            fig = plt.figure()
        ax1 = fig.add_subplot(2,1,1)
        ax2 = fig.add_subplot(2,1,2)
    else:
        try:
            fig1 = plt.figure(figsize=dict_print['figsize'])
            fig2 = plt.figure(figsize=dict_print['figsize'])
        except:
            fig1 = plt.figure()
            fig2 = plt.figure()
        ax1 = fig1.add_subplot(1,1,1)
        ax2 = fig2.add_subplot(1,1,1)

 
    if dict_print['subplots']:
        #ax2.set_xlabel(r"dS$_{omp}$[cm]")
        ax2.set_xlabel("dS,omp [cm]")
        plt.setp(ax1.get_xticklabels(), visible=False)
    else:
        ax1.set_xlabel("dS,omp [cm]")
        ax2.set_xlabel("dS,omp [cm]")

    ax1.set_ylabel("[Pa]"+dict_print['ylabel_1_extra'])
    ax2.set_ylabel("[Pa]"+dict_print['ylabel_2_extra'])


    # Fig 1/ sub 1: Pressure profiles and main sources
    ax1.axvline(0, color = 'k', lw=1)
    ax1.axhline(0, color = 'k', lw=1)
    ax1.plot(rmrsep[rind], ptot_up[rind]/spx_down[rind],'k',
             linewidth = 2, label='Total pressure upstream')
    ax1.plot(rmrsep[rind], psf_up[rind]/spx_down[rind],'k--',
             linewidth = 2, label='Static pressure upstream')
    ax1.plot(rmrsep[rind], ptot_down[rind]/spx_down[rind],'r',
             linewidth = 2, label='Total pressure downstream')
    ax1.plot(rmrsep[rind], psf_down[rind]/spx_down[rind],'r--',
             linewidth = 2, label='Static pressure downstream')
    if not missing_resmo:
        ax1.plot(rmrsep[rind],error_lin[rind]/spx_down[rind],
                 color = 'DarkViolet', linewidth = 2, label='Error')
        ax1.plot(rmrsep[rind], neutint[rind]/spx_down[rind],
             color='DarkGreen', linewidth = 2, label='Neutrals source')
    else:   
        ax1.plot(rmrsep[rind],np.zeros(len(rind)),
                 color = 'DarkViolet', linewidth = 2, 
                 label='Error*** (= 0.0)')
        ax1.plot(rmrsep[rind],-1.*error_lin[rind]/spx_down[rind], 
             color='DarkGreen', linewidth = 2, 
             label='Neutrals source*** (-Error)')
    ax1.plot(rmrsep[rind], othersource[rind]/spx_down[rind],
             color='Blue', linewidth = 2, label='Other sources')
    try:
        ax1.legend(loc='best', 
                prop={'size':dict_print['legend_font_size']})
    except:
        ax1.legend(loc='best')
    ax1.grid(True)

    ax1.set_xlim(dict_print['xlim_1'])
    ax1.set_ylim(dict_print['ylim_1'])

    if not dict_print['subplots'] and not dict_print['save']:
        fig1.tight_layout()            
    elif not dict_print['subplots'] and dict_print['save']:
        fig1.savefig(dict_print['save_path']+
                '/'+dict_print['save_name_1'],
                    bbox_inches='tight')

    # Fig 2/ sub 2: Details of sources
    ax2.axvline(0, color = 'k', lw=1)
    ax2.axhline(0, color = 'k', lw=1)
    if not missing_resmo:
        ax2.plot(rmrsep[rind], neutint[rind]/spx_down[rind],
             color='DarkGreen', linewidth = 2, label='Neutrals source')
    else:
        ax2.plot(rmrsep[rind],-1.*error_lin[rind]/spx_down[rind],
             color='DarkGreen', linewidth = 2, 
             label='Neutrals source*** (-Error)')

    ax2.plot(rmrsep[rind], (vis_up-vis_down)[rind]/spx_down[rind],'r',
             linewidth = 2, label='Parallel viscosity')
    ax2.plot(rmrsep[rind], raddivvint[rind]/spx_down[rind],
             color='b', linewidth = 2, label='Perpendicular viscosity')
    ax2.plot(rmrsep[rind], raddivdint[rind]/spx_down[rind],'k', 
             linewidth = 2, label=r'div(mnu_{par}u_{perp})')
    ax2.plot(rmrsep[rind],geomint[rind]/spx_down[rind],
             color = 'olive', linewidth = 2, label='Geom. term')
    ax2.plot(rmrsep[rind],smqsmavint[rind]/spx_down[rind],
             color = 'DarkGray', linewidth = 2, label='smq + smav')
    if not missing_resmo:
        ax2.plot(rmrsep[rind],error_lin[rind]/spx_down[rind],
                 color = 'DarkViolet', linewidth = 2, label='Error')
    else:
        ax2.plot(rmrsep[rind], np.zeros(len(rind)),
                 color = 'DarkViolet', linewidth = 2, 
                 label='Error*** (= 0.0)')
    ax2.plot(rmrsep[rind],resint[rind]/spx_down[rind],
             color = 'pink', linewidth = 2, label='Residuals')
    try:
        ax2.legend(loc='best',
            prop={'size':dict_print['legend_font_size']})
    except:
        ax2.legend(loc='best')
    ax2.grid(True)
    ax2.set_xlim(dict_print['xlim_2'])
    ax2.set_ylim(dict_print['ylim_2'])

    if not dict_print['subplots'] and not dict_print['save']:
        fig2.tight_layout()            
    elif dict_print['save']:
        fig2.savefig(dict_print['save_path']+
                '/'+dict_print['save_name_2'],
                    bbox_inches='tight')
    else:
        plt.tight_layout()

    if show:
        plt.show()

