#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Description:
    Provides methods for plotting and visualization of data.
"""

# Common modules:
import os
import logging
from matplotlib.backends.backend_pdf import PdfPages
from subprocess import Popen

# Solpspy modules:
from solpspy.tools.tools import try_block

# =============================================================================

class Pdfpages(object):

    def __init__(self,storagepath='.'):

        logging.basicConfig()
        self._log = logging.getLogger('Pdfpages Class')
        self._log.setLevel('INFO')

        self.set_storage_path(storagepath)

    def set_storage_path(self,storagepath):
        storagepath = os.path.abspath(storagepath)
        if os.path.isdir(storagepath):
            self.storagepath = storagepath
        else:
            self._log.error('Invalid path: %s',storagepath)

    def open(self,filename):
        """Opens an pdfpages object which allows to save all plots into
        a pdf file. If a pdfpages object was opened, all plots which are
        made are saved automatically."""
        self.close()
        if not filename.endswith('.pdf'): filename += '.pdf'
        self.filename = os.path.join(self.storagepath,filename)
        self.pdfpages = PdfPages(self.filename)

    def save(self):
        """Saves the current plot in the pdfpages object, if one was opened."""
        try:
            self.pdfpages.savefig()
        except:
            pass

    def close(self):
        """Closes the pdfpages object.
        REMARK: Don't forget this, or the pdf file will not be created!"""
        try:
            self.pdfpages.close()
            del self.pdfpages
            print("Pdf file is stored as '" + self.filename + "'.") 
        except:
            pass

    def show(self):
        self.close()
        try:
            Popen(['acroread',self.filename])
        except:
            Popen(['gv',self.filename])
