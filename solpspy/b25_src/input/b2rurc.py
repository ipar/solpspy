def b2rurc(mother):
    """
    Code extracted from input/b2rurc.F

        B2RURC reads a table of atomic rate data from an un*formatted file.

        This routine reads from unit (nget) a table of atomic rate data,
    covering ionisation, recombination, electron heat loss, charge exchange
    and radiation with neutral hydrogen. Unit nget must be connected to an
    un*formatted file. See the code to discern the required format of the
    input file.

        See routine b2cdca for explanation of 'un*formatted file'.


    Input
    -----
    nget (int)
        Specifies a unit number for un*formatted input.


    Output
    ------
    b2cmrc
        Table of atomic rate coefficients.


    Notes
    -----
    Internal parameters

     rtnt - integer constant.
    rtnt specifies the number of intervals in the discretisation
    of the temperature range. The table will contain data for
    (rtnt+1) values of the temperature variable.

     rtnn - integer constant.
    rtnn specifies the number of intervals in the discretisation
    of the density range. The table will contain data for (rtnn+1)
    values of the density variable.

     rtns - integer.
    rtns specifies the total number of atomic species; i.e.
      (sum inuc : 0.le.inuc.lt.nnuc : izhi(inuc)-izlo(inuc)+1) ,
    where inuc is a nuclear species index, izlo(inuc) is the lowest
    and izhi(inuc) is the highest atomic charge considered in b2 for
    that nuclear species.
    rtns equals the variable ns used elsewhere in b2.

     rtdumm - integer.
    rtdumm is a dummy integer variable, introduced in order to obtain
    an even-length sequence of integer variables in the common block.

     rtzmin, rtzmax, rtzn - (0:rtns-1) real*8 array.
    For 0.le.is.lt.rtns, rtzmin(is) and rtzmax(is) specify the atomic 
    charge range, and rtzn(is) specifies the nuclear charge
    of atomic species (is). 
    The izhi(inuc)-izlo(inuc)+1 charge states for each nuclear
    species are taken in natural order, starting at izlo(inuc); the
    highest state of nuclear species (inuc) is followed by the lowest
    state of (inuc+1). rtzmin, rtzmax and rtzn are equal to the arrays 
    zamin, zamax and zn used elsewhere in b2.

     rtt - (0:rtnt) real*8 array.
    rtt(0:rtnt) specifies the sequence of values of the temperature
    variable, expressed in ev, for which the rate coefficients are
    evaluated. rtt(0:rtnt) will be positive and increasing.

     rtn - (0:rtnn) real*8 array.
    rtn(0:rtnn) specifies the sequence of values of the density
    variable, expressed in m**-3, for which the rate coefficients
    are evaluated. rtn(0:rtnn) will be positive and increasing.

     rtlt - (0:rtnt) real*8 array.
    rtlt(0:rtnt) holds log(rtt(0:rtnt)).

     rtln - (0:rtnn) real*8 array.
    rtln(0:rtnn) holds log(rtn(0:rtnn)).

     rtlsa - (0:rtnt,0:rtnn,0:rtns-1) real*8 array.
    For (i,j,k) in (0:rtnt,0:rtnn,0:rtns-1), rtlsa(i,j,k) specifies
    logu(rtrsa), where rtrsa is the ionisation rate coefficient at
    electron temperature rtt(i), electron density rtn(j), for the
    process k->k+1. rtrsa has dimension m**3/s. The function logu is
    the same as (log) except that logu(0.0) is equal to some huge
    negative number.

     rtlra - (0:rtnt,0:rtnn,0:rtns-1) real*8 array.
    For (i,j,k) in (0:rtnt,0:rtnn,0:rtns-1), rtlra(i,j,k) specifies
    logu(rtrra), where rtrra is the recombination rate coefficient at
    electron temperature rtt(i) and electron density rtn(j), for the
    process k->k-1. rtrra has dimension m**3/s.

     rtlqa - (0:rtnt,0:rtnn,0:rtns-1) real*8 array.
    For (i,j,k) in (0:rtnt,0:rtnn,0:rtns-1), rtlqa(i,j,k) specifies
    logu(rtrqa), where rtrqa is the electron heat loss rate
    coefficient at electron temperature rtt(i) and electron density
    rtn(j), for processes starting from atomic species k. rtrqa has
    dimension eV*m**3/s.

     rtlcx - (0:rtnt,0:rtnn,0:rtns-1) real*8 array.
    For (i,j,k) in (0:rtnt,0:rtnn,0:rtns-1), rtlcx(i,j,k) specifies
    logu(rtrcx), where rtrcx is the charge exchange rate coefficient
    at neutral hydrogen temperature/amu rtt(i) and electron density
    rtn(j), for the process k->k-1. rtrcx has dimension m**3/s.

     rtlrd - (0:rtnt,0:rtnn,0:rtns-1) real*8 array.
    For (i,j,k) in (0:rtnt,0:rtnn,0:rtns-1), rtlrd(i,j,k) specifies
    logu(rtrrd), where rtrrd is the line radiation rate
    coefficient at electron temperature rtt(i) and electron density
    rtn(j), for processes starting from atomic species k. rtrrd has
    dimension eV*m**3/s.

     rtlbr - (0:rtnt,0:rtnn,0:rtns-1) real*8 array.
    For (i,j,k) in (0:rtnt,0:rtnn,0:rtns-1), rtlbr(i,j,k) specifies
    logu(rtrbr), where rtrbr is the bremsstrahlung radiation rate
    coefficient at electron temperature rtt(i) and electron density
    rtn(j), for processes starting from atomic species k. rtrbr has
    dimension eV*m**3/s.

     rtlza - (0:rtnt,0:rtnn,0:rtns-1) real*8 array.
    For (i,j,k) in (0:rtnt,0:rtnn,0:rtns-1), rtlza(i,j,k) specifies
    the effective charge state at
    electron temperature rtt(i), electron density rtn(j), for the
    superstage k. rtlza has dimension e. 

     rtlz2 - (0:rtnt,0:rtnn,0:rtns-1) real*8 array.
    For (i,j,k) in (0:rtnt,0:rtnn,0:rtns-1), rtlz2(i,j,k) specifies
    the effective squared charge at
    electron temperature rtt(i), electron density rtn(j), for the
    superstage k. rtlz2 has dimension e**2. 

     rtlpt - (0:rtnt,0:rtnn,0:rtns-1) real*8 array.
    For (i,j,k) in (0:rtnt,0:rtnn,0:rtns-1), rtlpt(i,j,k) specifies
    the cumulative ionisation potential at
    electron temperature rtt(i), electron density rtn(j), 
    to reach superstage k. rtlpt has dimension eV. 

     rtlpi - (0:rtnt,0:rtnn,0:rtns-1) real*8 array.
    For (i,j,k) in (0:rtnt,0:rtnn,0:rtns-1), rtlpi(i,j,k) specifies
    the effective ionisation potential at
    electron temperature rtt(i), electron density rtn(j), 
    to reach superstage k from superstage k-1. rtlpi has dimension eV. 



    """
