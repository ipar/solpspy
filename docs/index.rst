Solpspy: SOLPS post-processing accesible to all!
================================================

Solpspy is a Python 3 package which aims to ease post-processing of SOLPS
simulations for all type of people, from the beginner to the expert.

The source code can be found in: https://gitlab.com/ipar/solpspy

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   guides/quickstart
   api_reference
   installation
   guides/testing
   family 
   guides/plotting
   authors



..
    Indices and tables
    ==================
    
    * :ref:`genindex`
    * :ref:`modindex`
    * :ref:`search`
