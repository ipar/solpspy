import numpy as np

def intfacev(shot,vol, centre):
    nx = shot.nx
    ny = shot.ny
    bottomix = shot.bottomix
    bottomiy = shot.bottomiy

    face = np.zeros_like(centre)
    for ix in xrange(nx):
        for iy in xrange(ny):
            if bottomiy[ix,iy] == -1:
                face[ix,iy]=0.0
            else:
                face[ix,iy] =
                    (vol[ix,iy]*centre[bottomix[ix,iy],bottomiy[ix,iy]]+
              vol[bottomix[ix,iy],bottomiy[ix,iy]]*centre[ix,iy])/
             (vol[bottomix[ix,iy],bottomiy[ix,iy]]+vol[ix,iy])
    return face

def intfaceh(shot,vol, centre):
    nx = shot.nx
    ny = shot.ny
    leftix = shot.leftix
    leftiy = shot.leftiy

    face = np.zeros_like(centre)
    for ix in xrange(nx):
        for iy in xrange(ny):
            if leftiy[ix,iy] == -1:
                face[ix,iy]=0.0
            else:
                face[ix,iy] =
                    (vol[ix,iy]*centre[leftix[ix,iy],leftiy[ix,iy]]+
              vol[leftix[ix,iy],leftiy[ix,iy]]*centre[ix,iy])/
             (vol[leftix[ix,iy],leftiy[ix,iy]]+vol[ix,iy])
    return face
