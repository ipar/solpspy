#Spaces in --omit are important. Omit takes a single line with comma separated patterns that can
# have * (anything) or ? (any single character).

import os
from solpspy import module_path
import subprocess
#subprocess.call(
#    'coverage report --include="' +
#    os.path.join(module_path(),'*') + '" ' +
#    '--omit="*__init__*,*test_*,*/test/*,*mockups.py,*generate_data_fixtures*,*main_pytest.py"',
#    shell=True)

subprocess.call(
    'coverage report --include="' +
    os.path.join(module_path(),'*') + '" ' +
    '--omit="*__init__*,*test_*,*/test/*,*mockups.py,*generate_data_fixtures*,*main_pytest.py" ' +
    '--sort=cover',
    shell=True)
