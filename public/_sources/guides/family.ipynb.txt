{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "ad9c2531",
   "metadata": {},
   "source": [
    "# Databases with solpspy"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d7836f8a",
   "metadata": {},
   "source": [
    "For basic syntax, go to `quickstart`. In this notebook, a basic understanding of both the Python and the Solpspy syntax is assumed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e9261774",
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import os\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import itertools\n",
    "from collections import OrderedDict\n",
    "import copy\n",
    "\n",
    "import solpspy\n",
    "\n",
    "## Real test path\n",
    "#test_path = os.path.join(solpspy.module_path(), 'tests')\n",
    "\n",
    "## Test path for now, until real testing is created\n",
    "index = os.path.join(solpspy.module_path(),'../tests/new_testing_family/index.yaml')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "24a99605",
   "metadata": {},
   "source": [
    "The 5 seconds API rundown is the following. The result is an object (db in the example), which contains the parsed databases following the criteria set in ``include`` and ``exclude``, and which entries are ordered arcording to ``order``."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4f158c35",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "## The experiments should be given in order, but it only works sometimes, why??\n",
    "include = [{'experiment': ['just testing', 'another testing', 'refs testing']}]\n",
    "#include = [{'experiment': ['refs testing', 'just testing']}]\n",
    "\n",
    "#order = ['experiment', 'ip', 'fhecore']\n",
    "#order = ['experiment', {'ip':False}, 'fhecore'] #Correctly makes ip descending.\n",
    "#order = ['experiment', {'ip':False, 'teomp_avg':True}] # As expected.\n",
    "order = [{'experiment':['another testing', 'just testing','refs testing'],\n",
    "          'ip':False, 'teomp_avg':True}] # As expected.\n",
    "\n",
    "\n",
    "exclude = []\n",
    "#exclude = [{'ip':0.6}]\n",
    "\n",
    "data = solpspy.Family(include=include, exclude=exclude, order=order, index=index)\n",
    "print(data)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "50518dfd",
   "metadata": {},
   "source": [
    "## Changing order"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f2a608f5",
   "metadata": {},
   "outputs": [],
   "source": [
    "data2 = copy.deepcopy(data)\n",
    "data2.order = ['experiment','teomp_avg', {'ip':True}]\n",
    "#data2.order = [{'experiment': ['another testing', 'refs testing','just testing']}, \n",
    "#                'teomp_avg', {'ip':True}]\n",
    "print(data2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cbcb4bf5",
   "metadata": {},
   "outputs": [],
   "source": [
    "data.runs[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "beac6638",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "data3 = copy.deepcopy(data)\n",
    "data3.order = [{'experiment': ['another testing', 'refs testing','just testing'],\n",
    "                'ip': True}, 'teomp_avg']\n",
    "print(data3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a8ada7b7",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "data4 = copy.deepcopy(data)\n",
    "data4.order = [{'experiment': ['another testing', 'refs testing','just testing'],\n",
    "                #'ip': [1.0, 0.6, 0.8]},\n",
    "               }, 'ip',\n",
    "                'teomp_avg']\n",
    "print(data4)\n",
    "# Wrong: For some reason it is making 0.6, 1.0, 0.8?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "776c875c",
   "metadata": {},
   "source": [
    "## Experimental stuff"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
