__version__ = '0.1.0'




import logging
slog = logging.getLogger('solpspy')
if not slog.handlers: #Prevents from adding repeated streamhandlers
    formatter = logging.Formatter(
            '%(name)s -- %(levelname)s: %(message)s')
    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    slog.addHandler(ch)
    slog.propagate = False #Otherwise, root logger also prints




## Classes
from .factory import SolpsData
#from .classes.mds import MdsData
#from .classes.rundir import RundirData
from .classes.family import Family

## Plots
from .plots.plot_class import VesselPlot
from .plots.plot_class import GridDataPlot
from .plots.plot_class import ComputationalDataPlot
from .plots.plot_class import TriangDataPlot

## Tools
from .tools.tools import get_rhopol
from .tools.tools import module_path


__all__ = [
    'SolpsData',
    'Family',
    'VesselPlot',
    'GridDataPlot',
    'ComputationalDataPlot',
    'TriangDataPlot',
    'get_rhopol',
    'module_path'
]











