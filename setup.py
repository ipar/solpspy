import setuptools

setuptools.setup(
    name='solpspy',
    version='0.1.0', #Not going to work, copy eg jupyter-sphinx setup.py
    author='Ivan Paradela Perez',
    url='https://gitlab.com/ipar/solpspy',
    description='Python 3 package to work with SOLPS simulations',
    #packages = setuptools.find_packages(include=['solpspy'])
    packages = ['solpspy'],
    python_requires='>=3.7', # Older versions have not been tested much.

## TOO NEW FOR SOME PEOPLE
#    install_requires=[
#        'numpy>=1.20.2',
#        'scipy>=1.6.2',
#        'matplotlib >= 3.4.1',
#        'pandas>=1.2.3',
#        'tqdm>=4.51.0',
#        'PyYAML>=5.4.1',
#        'pytest>=6.2.2'
#        ],
#
#    extras_require={
#        #For documentation
#        'docs':[ 
#            'sphinx>=1.4',
#            'ipykernel==5.5.3',
#            'nbsphinx==0.8.7',
#            'jupyter-sphinx==0.3.2',
#            'sphinx_rtd_theme==0.5.2'
#        ]

#Try this and set compatibilities as they come in
    install_requires=[
        'numpy',
        'scipy',
        'matplotlib',
        'pandas',
        'tqdm',
        'PyYAML',
        'pytest',
        'coverage',
        ],

    extras_require={
        #For documentation
        'docs':[ 
            'sphinx',
            'ipykernel',
            'nbsphinx',
            'jupyter-sphinx',
            'sphinx_rtd_theme',
        ]
    }
)
