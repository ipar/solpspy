import numpy as np
import itertools

from solpspy.tools.tools import solps_property, try_block, priority
from solpspy.tools.tools import generate_mask


### Maybe factorize into BaskMasks with generate, regions, and the basics


class BaseMasks(object):

    def __init__(self,mother):
        self._mother = mother
        self.log = self._mother.log
        self.irlcl = True


    @solps_property
    def gdcl(self):
        """(nx, ny): Guard cells = True."""
        self._gdcl = np.zeros((self._mother.nx,self._mother.ny), dtype=bool)

        self._gdcl[0] = 1
        self._gdcl[-1] = 1
        self._gdcl[:,0] = 1
        self._gdcl[:,-1] = 1

    @property
    def guard_cells(self):
        """Alias for :py:attr:`~gdcl`"""
        return self.gdcl

    @solps_property
    def rlcl(self):
        """(nx, ny): Real cells = True."""
        self._rlcl = np.logical_not(self.gdcl)

    @property
    def real_cells(self):
        """Alias for :py:attr:`~rlcl`"""
        return self.rlcl


    def regions(self, regions, mode='combination', rlcl=True):
        """

        Parameters
        ----------

        regions: {'v', 'x', 'y'} dict
            Type of region: Volume, x-directed, or y-directed.
            The associated list contains the indices to be found in said
            type of region.
            Important: index of region is not Python index (starts at 1).

        mode: {'combination', 'intersection'}, optional
            Combination or intersection of all given the regions and indices.

        rlcl: bool, optional
            Whether or not only real cells should be considered.

        Returns
        -------
        (nx, ny)
            Boolean array with True wherever mode applies to the given regions.
        """

        str_to_reg = {'v':0, 'x':1, 'y':2}

        if mode == 'combination':
            mask = np.zeros((self._mother.nx, self._mother.ny))
        elif mode == 'intersection':
            mask = np.ones((self._mother.nx, self._mother.ny))

        for d, inds in list(regions.items()):
            if isinstance(inds,int) or isinstance(inds,float):
                inds = [int(inds)]
            if isinstance(d,str):
                d = str_to_reg[d]
            for ind in inds:
                tmp = self._mother.region[...,d] == ind
                if mode == 'combination':
                    mask = np.logical_or(mask, tmp)
                if mode == 'intersection':
                    mask = np.logical_and(mask, tmp)

        if rlcl:
            return np.logical_and(mask,self.rlcl)
        else:
            return mask


    #Core, SOL, inndiv and outdiv to be defined in subclasses.

    @solps_property
    def notcore(self):
        """(nx, ny): All real cells except the core = True.
        """
        self._notcore = self.rlcl*(1-self.core)


    #@solps_property
    #def pfr(self):
    #    self._pfr = (1-self.sol) * self.notcore * self.rlcl

    @solps_property
    def pfr(self):
        """(nx, ny): Real cells in the private flux region.
        """
        self._pfr = np.logical_and(self.real_cells,
                np.logical_and(self.notcore,
                    np.logical_not(self.sol)))


    @solps_property
    def div(self):
        """(nx, ny): Real cells in the main inner + 0uter divertors.
        """
        self._div = self.inndiv + self.outdiv

    @property
    def divertor(self):
        """Alias of :py:attr:`~div`"""
        return self.div

    @solps_property
    def notdiv(self):
        """(nx, ny): Every real cell  except inner+outer divertors.
        """
        self._notdiv = self.rlcl*(
                np.ones((self._mother.nx,self._mother.ny))-self.div)


    @solps_property
    def innertarget(self):
        """(nx, ny): Real cells along the inner diveror are True.
        """
        self._innertarget = np.zeros((self._mother.nx,self._mother.ny))
        self._innertarget[self._mother.iinn,:] = 1

    @solps_property
    def outertarget(self):
        """(nx, ny): Real cells along the outer diveror are True.
        """
        self._outertarget = np.zeros((self._mother.nx,self._mother.ny))
        self._outertarget[self._mother.iout,:] = 1



    def generate(self, **kwargs):
        """ Pluggin for tools.core.generate."""
        return generate_mask(self._mother, **kwargs)

    def __call__(self, **kwargs):
        return self.generate(**kwargs)


    ## ATTENTION 2021: Is logic correctly applied??
    @property
    @try_block
    def xpoint(self):
        """Grid mask for the grid cells surrounding the x-point."""
        self._xpoint = np.zeros((self._mother.nx,self._mother.ny))
        for nx in range(self._mother.nx):
            for ny in range(self._mother.ny):
                cell = self._mother.grid[nx,ny] # [nvertex,R|z]
                for v in range(len(cell)):
                    if (cell[v][0] == self._mother.xpoint[0] and
                        cell[v][1] == self._mother.xpoint[1]):
                        self._xpoint[nx,ny] = 1







########## ====================================================================


class MasksLSN(BaseMasks):

    def __init__(self,mother):
        super().__init__(mother)

    ## Region definitions
    @solps_property
    def core(self):
        """ (nx, ny): Real cells in the core region = True."""
        self._core = self.regions({'v':1}, rlcl=self.irlcl)

    ## DEFINITION CHANGED: Before it was SOL including in divs.
    ## Now only SOL above divs. This one is better??
    ## Maybe call it main_sol?
    @solps_property
    def mainsol(self):
        """ (nx, ny): Real cells in the main (outside of div) SOL region = True."""
        self._mainsol = self.regions({'v':2}, rlcl=self.irlcl)

    @solps_property
    def sol(self):
        """ (nx, ny): Real cells in the SOL region = True."""
        self._sol = np.logical_and(self.real_cells,
                self.generate(yrange=[self._mother.sep, self._mother.ny]))

    @solps_property
    def outdiv(self):
        """ (nx, ny): Real cells in the outer divertor region = True."""
        self._outdiv = self.regions({'v':4}, rlcl=self.irlcl)

    @solps_property
    def inndiv(self):
        """ (nx,ny): Real cells inner divertor region = True."""
        self._inndiv = self.regions({'v':3}, rlcl=self.irlcl)








########## ====================================================================


class MasksUSN(BaseMasks):

    def __init__(self,mother):
        super().__init__(mother)


    ## Region definitions
    @solps_property
    def core(self):
        """ (nx,ny): Core region."""
        self._core = self.regions({'v':1}, rlcl=self.irlcl)

    @solps_property
    def sol(self):
        """ (nx,ny): SOL region."""
        self._sol = self.regions({'v':2}, rlcl=self.irlcl)

    @solps_property
    def outdiv(self):
        """ (nx,ny): Outer divertor region."""
        #TO BE TESTED
        self._outdiv = self.regions({'v':3}, rlcl=self.irlcl)

    @solps_property
    def inndiv(self):
        """ (nx,ny): Inner divertor region."""
        #TO BE TESTED
        self._inndiv = self.regions({'v':4}, rlcl=self.irlcl)












########## ====================================================================


class MasksDDNU(BaseMasks):
    """Class containing different grid masks."""

    def __init__(self,mother):
        super().__init__(mother)

    @solps_property
    def gdcl(self):
        """(nx,ny): Guard cells."""
        self._gdcl = np.zeros((self._mother.nx,self._mother.ny))

        self._gdcl[0] = 1
        self._gdcl[-1] = 1
        self._gdcl[:,0] = 1
        self._gdcl[:,-1] = 1
        self._gdcl[self._mother.iout-1,:] = 1
        self._gdcl[self._mother.iinn+1,:] = 1



    ## Region definitions
    # Core
    @solps_property
    def inncore(self):
        """ (nx,ny): Inner core region."""
        self._inncore = self.regions({'v':1}, rlcl=self.irlcl)

    @solps_property
    def outcore(self):
        """ (nx,ny): Outer core region."""
        self._outcore = self.regions({'v':5}, rlcl=self.irlcl)

    @solps_property
    def core(self):
        self._core = np.logical_or(self.outcore, self.inncore)


    ## ATTENTION: These are still not the complete SOLs.
    # Sol
    @solps_property
    def outsol(self):
        """ (nx,ny): Outer (complete) SOL region."""
        self._outsol = self.regions({'v':6}, rlcl=self.irlcl)

    @solps_property
    def innsol(self):
        """ (nx,ny): Inner (complete) SOL region."""
        self._innsol = self.regions({'v':2}, rlcl=self.irlcl)

    @solps_property
    def sol(self):
        """ (nx,ny): Complete SOL region."""
        self._sol = np.logical_or(self.outsol, self.innsol)

    #Create sol1 (only main), sol2(only secondary)



    # Divertor
    @solps_property
    def outdiv(self):
        """ (nx,ny): Main outer divertor region."""
        self._outdiv = self.regions({'v':7}, rlcl=self.irlcl)

    @solps_property
    def inndiv(self):
        """ (nx,ny): Main inner divertor region."""
        self._inndiv = self.regions({'v':4}, rlcl=self.irlcl)


    # Secondary Divertor
    @solps_property
    def outdiv2(self):
        """ (nx,ny): Secondary outer divertor region."""
        self._outdiv2 = self.regions({'v':8}, rlcl=self.irlcl)

    @solps_property
    def inndiv2(self):
        """ (nx,ny): Secondary inner divertor region."""
        self._inndiv2 = self.regions({'v':3}, rlcl=self.irlcl)

    @solps_property
    def div2(self):
        """ Secondary Inner + Outer divertors.
        Guard cells are excluded.
        """
        self._div2 = self.inndiv2 + self.outdiv2

    @property
    def divertor2(self):
        """Alias of self.div"""
        return self.div2

    @solps_property
    def notdiv2(self):
        """ Everything except inner+outer divertors.
        Guard cells are excluded.
        """
        self._notdiv2 = self.rlcl*(
                np.ones((self._mother.nx,self._mother.ny))-self.div2)


    @solps_property
    def innertarget2(self):
        self._innertarget2 = np.zeros((self._mother.nx,self._mother.ny))
        self._innertarget2[self._mother.iinn2,:] = 1 # nx=1 (rlcl)

    @solps_property
    def outertarget2(self):
        self._outertarget2 = np.zeros((self._mother.nx,self._mother.ny))
        self._outertarget2[self._mother.iout2,:] = 1




    @solps_property
    def hfs(self):
        """ High field side region of grid.
        """
        self._hfs = np.logical_and(self.real_cells,
                self.regions({'v':[1,2,3,4]}))
                    
    @solps_property
    def lfs(self):
        """ Low field side region of grid.
        """
        self._lfs = np.logical_and(self.real_cells,
                self.regions({'v':[5,6,7,8]}))
                    


    ## LOGIC IS NOT CORRECTLY APPLIED
    @property
    @try_block
    def xpoint(self):
        """Grid mask for the grid cells surrounding the x-point."""
        self._xpoint = np.zeros((self._mother.nx,self._mother.ny))
        for nx in range(self._mother.nx):
            for ny in range(self._mother.ny):
                cell = self._mother.grid[nx,ny] # [nvertex,R|z]
                for v in range(len(cell)):
                    if (cell[v][0] == self._mother.xpoint[0] and
                        cell[v][1] == self._mother.xpoint[1]):
                        self._xpoint[nx,ny] = 1







########## ====================================================================

class MasksCDN(MasksDDNU):
    def __init__(self, mother):
        super().__init__(mother)

    @solps_property
    def gdcl(self):
        """(nx,ny): Guard cells."""
        self._gdcl = np.zeros((self._mother.nx,self._mother.ny))

        self._gdcl[self._mother.iout+1] = 1
        self._gdcl[self._mother.iinn-1] = 1
        self._gdcl[self._mother.iout2-1,:] = 1
        self._gdcl[self._mother.iinn2+1,:] = 1
        self._gdcl[:,0] = 1
        self._gdcl[:,-1] = 1


    #Core, sol, inndiv and outdiv 'v' regions are the same as DDNU.
    # The only change is what is considered main

    # Divertor
    @solps_property
    def outdiv(self):
        """ (nx,ny): Lower outer divertor region."""
        self._outdiv = self.regions({'v':8}, rlcl=self.irlcl)

    @solps_property
    def inndiv(self):
        """ (nx,ny): Lower inner divertor region."""
        self._inndiv = self.regions({'v':3}, rlcl=self.irlcl)


    # Secondary Divertor
    @solps_property
    def outdiv2(self):
        """ (nx,ny): Secondary outer divertor region."""
        self._outdiv2 = self.regions({'v':7}, rlcl=self.irlcl)

    @solps_property
    def inndiv2(self):
        """ (nx,ny): Secondary inner divertor region."""
        self._inndiv2 = self.regions({'v':4}, rlcl=self.irlcl)
