import os
from solpspy import module_path
import subprocess
subprocess.call(
    'coverage html --include="' +
    os.path.join(module_path(),'*') + '" ' +
    '--omit="*__init__*,*test_*,*/test/*,*mockups.py,*generate_data_fixtures*,*main_pytest.py"',
    shell=True)
subprocess.call('firefox --new-window htmlcov/index.html', shell=True)
