:orphan:

Plot classes
------------

.. autoclass:: plots.plot_class.BasePlot
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:

.. autoclass:: plots.plot_class.VesselPlot
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:

.. autoclass:: plots.plot_class.GridDataPlot
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:

.. autoclass:: plots.plot_class.ComputationalDataPlot
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:

.. autoclass:: plots.plot_class.TriangDataPlot
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
