:orphan:

Chords class
------------

.. autoclass:: extensions.chords.Chords
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
