def main_func():
    import os
    from IPython import embed
    from solpspy import MdsData
    from solpspy import RundirData
    from solpspy import SolpsData
    import numpy as np
    import matplotlib.pyplot as plt
    import pdb

    #shot = MdsData(94908) # Saved amds SOLPS5.0 for SOLPS-ITER run
    shot = SolpsData(119603, maggeom='ddnu') # SOLPS-ITER ddnu magnetic geometry

    if not os.path.exists(str(shot.shot)):
            os.makedirs(str(shot.shot))

    #For test_mds_properties.py
    np.save(str(shot.shot)+'/user', shot.user)
    np.save(str(shot.shot)+'/solpsversion', shot.solpsversion)
    np.save(str(shot.shot)+'/vessel', shot.vessel)
    np.save(str(shot.shot)+'/region', shot.region)
    np.save(str(shot.shot)+'/regflx', shot.regflx)
    np.save(str(shot.shot)+'/regfly', shot.regfly)
    np.save(str(shot.shot)+'/regvol', shot.regvol)
    np.save(str(shot.shot)+'/cr', shot.cr)
    np.save(str(shot.shot)+'/cr_y', shot.cr_y)
    np.save(str(shot.shot)+'/cz', shot.cz)
    np.save(str(shot.shot)+'/r', shot.r)
    np.save(str(shot.shot)+'/z', shot.z)
    np.save(str(shot.shot)+'/nx', shot.nx)
    np.save(str(shot.shot)+'/ny', shot.ny)
    np.save(str(shot.shot)+'/ns', shot.ns)
    np.save(str(shot.shot)+'/natm', shot.natm)
    np.save(str(shot.shot)+'/nmol', shot.nmol)
    np.save(str(shot.shot)+'/imp', shot.imp)
    np.save(str(shot.shot)+'/omp', shot.omp)
    np.save(str(shot.shot)+'/sep', shot.sep)
    np.save(str(shot.shot)+'/za', shot.za)
    np.save(str(shot.shot)+'/am', shot.am)
    np.save(str(shot.shot)+'/b', shot.b)
    np.save(str(shot.shot)+'/pitch', shot.pitch)
    np.save(str(shot.shot)+'/pitch_pt', shot.pitch_pt)
    np.save(str(shot.shot)+'/pitch_tot', shot.pitch_tot)
    np.save(str(shot.shot)+'/invpitch', shot.invpitch)
    np.save(str(shot.shot)+'/dv', shot.dv)
    np.save(str(shot.shot)+'/hx', shot.hx)
    np.save(str(shot.shot)+'/hy', shot.hy)
    np.save(str(shot.shot)+'/parallel_surface_m', shot.parallel_surface_m)
    np.save(str(shot.shot)+'/parallel_surface', shot.parallel_surface)
    np.save(str(shot.shot)+'/dsrad', shot.dsrad)
    np.save(str(shot.shot)+'/dspar', shot.dspar)
    np.save(str(shot.shot)+'/ipardspar', shot.ipardspar)
    np.save(str(shot.shot)+'/leftix', shot.leftix)
    np.save(str(shot.shot)+'/leftiy', shot.leftiy)
    np.save(str(shot.shot)+'/rightix', shot.rightix)
    np.save(str(shot.shot)+'/rightiy', shot.rightiy)
    np.save(str(shot.shot)+'/bottomix', shot.bottomix)
    np.save(str(shot.shot)+'/bottomiy', shot.bottomiy)
    np.save(str(shot.shot)+'/topix', shot.topix)
    np.save(str(shot.shot)+'/topiy', shot.topiy)

    np.save(str(shot.shot)+'/te', shot.te)
    np.save(str(shot.shot)+'/ti', shot.ti)
    np.save(str(shot.shot)+'/ne', shot.ne)
    np.save(str(shot.shot)+'/na', shot.na)
    np.save(str(shot.shot)+'/tab2', shot.tab2)
    np.save(str(shot.shot)+'/dab2', shot.dab2)
    np.save(str(shot.shot)+'/tmb2', shot.tmb2)
    np.save(str(shot.shot)+'/dmb2', shot.dmb2)
    np.save(str(shot.shot)+'/ua', shot.ua)
    np.save(str(shot.shot)+'/zeff', shot.zeff)
    np.save(str(shot.shot)+'/pstot', shot.pstot)
    np.save(str(shot.shot)+'/pdynd', shot.pdynd)

    np.save(str(shot.shot)+'/fna', shot.fna)
    np.save(str(shot.shot)+'/fna32', shot.fna32)
    np.save(str(shot.shot)+'/fna52', shot.fna52)

    np.save(str(shot.shot)+'/fnax32', shot.fnax32)
    np.save(str(shot.shot)+'/fnax52', shot.fnax52)
    np.save(str(shot.shot)+'/fnay32', shot.fnay32)
    np.save(str(shot.shot)+'/fnay52', shot.fnay52)

    np.save(str(shot.shot)+'/smo', shot.smo)
    np.save(str(shot.shot)+'/smq', shot.smq)
    np.save(str(shot.shot)+'/smav', shot.smav)
    np.save(str(shot.shot)+'/fmox', shot.fmox)
    np.save(str(shot.shot)+'/fmoy', shot.fmoy)
    np.save(str(shot.shot)+'/fnax', shot.fnax)
    np.save(str(shot.shot)+'/fnay', shot.fnay)
    np.save(str(shot.shot)+'/resmo', shot.resmo)
    np.save(str(shot.shot)+'/grid', shot.grid)
    np.save(str(shot.shot)+'/ds', shot.ds)
    np.save(str(shot.shot)+'/ixp', shot.ixp)
    np.save(str(shot.shot)+'/oxp', shot.oxp)

    np.save(str(shot.shot)+'/oul', shot.oul)
    np.save(str(shot.shot)+'/odl', shot.odl)
    np.save(str(shot.shot)+'/iul', shot.iul)
    np.save(str(shot.shot)+'/idl', shot.idl)
    np.save(str(shot.shot)+'/masks.outdiv', shot.masks.outdiv)
    np.save(str(shot.shot)+'/masks.inndiv', shot.masks.inndiv)
    np.save(str(shot.shot)+'/masks.notcore', shot.masks.notcore)
    np.save(str(shot.shot)+'/masks.sol', shot.masks.sol)
    np.save(str(shot.shot)+'/outdiv_nolim', shot.outdiv_nolim)
    np.save(str(shot.shot)+'/dab2_avg', shot.dab2_avg)
    np.save(str(shot.shot)+'/na_avg', shot.na_avg)
    np.save(str(shot.shot)+'/dmb2_avg', shot.dmb2_avg)
    np.save(str(shot.shot)+'/dab2_line', shot.dab2_line)
    np.save(str(shot.shot)+'/na_line', shot.na_line)
    np.save(str(shot.shot)+'/dmb2_line', shot.dmb2_line)


    #test_mds_timetraces.py
    np.save(str(shot.shot)+'/targ4', shot.targ4)
    np.save(str(shot.shot)+'/targ1', shot.targ1)

    np.save(str(shot.shot)+'/out.ds', shot.out.ds)
    np.save(str(shot.shot)+'/out.ft', shot.out.ft)
    np.save(str(shot.shot)+'/out.fi', shot.out.fi)
    np.save(str(shot.shot)+'/out.te', shot.out.te)
    np.save(str(shot.shot)+'/out.ne', shot.out.ne)
    np.save(str(shot.shot)+'/out.ti', shot.out.ti)
    np.save(str(shot.shot)+'/out.po', shot.out.po)
    np.save(str(shot.shot)+'/out.fc', shot.out.fc)

    np.save(str(shot.shot)+'/inn.ds', shot.inn.ds)
    np.save(str(shot.shot)+'/inn.ft', shot.inn.ft)
    np.save(str(shot.shot)+'/inn.fi', shot.inn.fi)
    np.save(str(shot.shot)+'/inn.te', shot.inn.te)
    np.save(str(shot.shot)+'/inn.ne', shot.inn.ne)
    np.save(str(shot.shot)+'/inn.ti', shot.inn.ti)
    np.save(str(shot.shot)+'/inn.po', shot.inn.po)
    np.save(str(shot.shot)+'/inn.fc', shot.inn.fc)



    np.save(str(shot.shot)+'/outmp.ds', shot.outmp.ds)
    np.save(str(shot.shot)+'/outmp.te', shot.outmp.te)
    np.save(str(shot.shot)+'/outmp.ne', shot.outmp.ne)
    np.save(str(shot.shot)+'/outmp.ti', shot.outmp.ti)

    np.save(str(shot.shot)+'/innmp.ds', shot.innmp.ds)
    np.save(str(shot.shot)+'/innmp.te', shot.innmp.te)
    np.save(str(shot.shot)+'/innmp.ne', shot.innmp.ne)
    np.save(str(shot.shot)+'/innmp.ti', shot.innmp.ti)


    if shot._type == 'MdsDataDDNU':
        np.save(str(shot.shot)+'/sep2', shot.sep2)
        # Implement correctly after time traces of all the targets
        np.save(str(shot.shot)+'/out2.ds', shot.out2.ds)
        np.save(str(shot.shot)+'/out2.ft', shot.out2.ft)
        np.save(str(shot.shot)+'/out2.fi', shot.out2.fi)
        np.save(str(shot.shot)+'/out2.te', shot.out2.te)
        np.save(str(shot.shot)+'/out2.ne', shot.out2.ne)
        np.save(str(shot.shot)+'/out2.ti', shot.out2.ti)
        np.save(str(shot.shot)+'/out2.po', shot.out2.po)
        np.save(str(shot.shot)+'/out2.fc', shot.out2.fc)

        np.save(str(shot.shot)+'/inn2.ds', shot.inn2.ds)
        np.save(str(shot.shot)+'/inn2.ft', shot.inn2.ft)
        np.save(str(shot.shot)+'/inn2.fi', shot.inn2.fi)
        np.save(str(shot.shot)+'/inn2.te', shot.inn2.te)
        np.save(str(shot.shot)+'/inn2.ne', shot.inn2.ne)
        np.save(str(shot.shot)+'/inn2.ti', shot.inn2.ti)
        np.save(str(shot.shot)+'/inn2.po', shot.inn2.po)
        np.save(str(shot.shot)+'/inn2.fc', shot.inn2.fc)


    #Latest additions. Locate in better positions
    np.save(str(shot.shot)+'/vessel_mesh', shot.vessel_mesh)
    np.save(str(shot.shot)+'/nnreg', shot.nnreg)
    np.save(str(shot.shot)+'/xpoint', shot.xpoint)
    np.save(str(shot.shot)+'/solps_code', shot.solps_code)
    np.save(str(shot.shot)+'/exp', shot.exp)

    np.save(str(shot.shot)+'/nncut', shot.nncut)
    np.save(str(shot.shot)+'/rightcut', shot.rightcut)
    np.save(str(shot.shot)+'/leftcut', shot.leftcut)
    np.save(str(shot.shot)+'/topcut', shot.topcut)
    np.save(str(shot.shot)+'/bottomcut', shot.bottomcut)

    np.save(str(shot.shot)+'/sy', shot.sy)
    np.save(str(shot.shot)+'/sx', shot.sx)
    np.save(str(shot.shot)+'/gs', shot.gs)

    np.save(str(shot.shot)+'/am_kg', shot.am_kg)
    np.save(str(shot.shot)+'/mi', shot.mi)

    np.save(str(shot.shot)+'/na_eirene', shot.na_eirene)
    np.save(str(shot.shot)+'/na_atom', shot.na_atom)

    np.save(str(shot.shot)+'/qz', shot.qz)

    np.save(str(shot.shot)+'/fractional_abundances', shot.fractional_abundances)
    np.save(str(shot.shot)+'/cimp', shot.cimp)

    np.save(str(shot.shot)+'/ni', shot.ni)

    np.save(str(shot.shot)+'/pdyna', shot.pdyna)
    np.save(str(shot.shot)+'/ptot', shot.ptot)

    np.save(str(shot.shot)+'/pressure_gradient_force', shot.pressure_gradient_force)

    np.save(str(shot.shot)+'/fhjx', shot.fhjx)
    np.save(str(shot.shot)+'/fhjy', shot.fhjy)
    np.save(str(shot.shot)+'/fhj', shot.fhj)
    np.save(str(shot.shot)+'/fhmx', shot.fhmx)
    np.save(str(shot.shot)+'/fhmy', shot.fhmy)
    np.save(str(shot.shot)+'/fhm', shot.fhm)
    np.save(str(shot.shot)+'/fhpx', shot.fhpx)
    np.save(str(shot.shot)+'/fhpy', shot.fhpy)
    np.save(str(shot.shot)+'/fhp', shot.fhp)
    np.save(str(shot.shot)+'/fhtx', shot.fhtx)
    np.save(str(shot.shot)+'/fhty', shot.fhty)
    np.save(str(shot.shot)+'/fht', shot.fht)

    np.save(str(shot.shot)+'/stored_energy', shot.stored_energy)

    np.save(str(shot.shot)+'/dperp', shot.dperp)
    np.save(str(shot.shot)+'/kyeperp', shot.kyeperp)
    np.save(str(shot.shot)+'/kyiperp', shot.kyiperp)

    np.save(str(shot.shot)+'/electric_field', shot.electric_field)
    np.save(str(shot.shot)+'/electric_force', shot.electric_force)














    #embed()


if __name__ == '__main__':
     #Can be implemented as decorator, I think
     try:
        main_func()
     except:
        import pdb, traceback, sys
        type, value, tb = sys.exc_info()
        traceback.print_exc()
        pdb.post_mortem(tb)
