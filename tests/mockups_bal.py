import os
import numpy as np
import pytest
import scipy.io as io
from solpspy.tools.tools import module_path, yaml_loader, SimpleNamespace
from solpspy.extensions.moulton_balance import ParticleBalance


@pytest.fixture(scope="session", params=['moulton_2317'])
def balpar(request):
    tmp_path = os.path.join(module_path,
            'tests/balance_mat/{0}'.format(request.param))
    inrad_mask = indrad_mask = io.loadmat(
            os.path.join(tmp_path,'/masks.mat')['indrad']

    dummy = ParticleBalance(tmp_path, gmask=inrad_mask, plotting=False)
    dummy._fixture_name = request.param
    return dummy


@pytest.fixture(scope="session")
def balpar_mat(balpar):
    tmp_path = os.path.join(module_path,
            'tests/balance_mat/{0}'.format(request.param))
    comuse_path = os.path.join(tmp_path, 'comuse.mat')
    comuse = io.loadmat(comuse_path)['comuse']
    dummy = SimpleNamespace()
    for k,v in comuse.iteritems():
        setattr(dummy, k, v)
    return dummy



#--------------------- Validation tools -----------------------------------
def validity_bal(balpar,var, indspec=1):
    tmp_path = os.path.join(module_path,'tests/balance_mat/'
    bal_var_mat = os.path.join(tmp_path,
        '{0}/{1}.mat'.format(balpar._fixture_name))
    dummy = io.loadmat(bal_var_mat)





    try:
        numvar = getattr(mds,var)
    except:
        try:
            var = var.split('.')
            numvar = getattr(getattr(mds,var[0]),var[1])
        except:
            return False

    if numvar is None:
        try:
            return (dummy == np.array(None))
        except:
            return False
    else:
        try:
            return np.allclose(numvar,dummy)
        except:
            try:
                return (numvar == dummy).all()
            except:
                try:
                    return (numvar == dummy)
                except:
                    return False





##--------------------- Skipping   tools -----------------------------------
#def skip_by_species(sim, ns):
#    if sim.ns < ns:
#        pytest.skip('Number of fluid species is just {}.'.format(str(sim.ns)))
#
#def skip_by_especies(sim, natm):
#    if sim.natm < natm:
#        pytest.skip('Number of Eirene species is just {}.'.format(str(sim.natm)))
#
#def skip_b2standalone(sim):
#    if sim.natm is None and sim.nmol is None and sim.nion is None:
#        pytest.skip('B2standalone simulation.')
#
#
#def skip_ddn(sim):
#    ddn_types =['MdsDataDDNU', 'RundirDataDDNU']
#    if sim._type in ddn_types:
#        pytest.skip('Test not for DDN simulations.')
#
#def skip_sn(sim):
#    ddn_types =['MdsDataDDNU', 'RundirDataDDNU']
#    if sim._type not in ddn_types:
#        pytest.skip('Test only for DDN features.')
#
#
#def skip_no_mdsplus_module(sim):
#    try:
#        import MDSplus
#        #sys.modules['MDSplus']
#    except:
#        pytest.skip('Test only for DDN features.')
