#!/usr/bin/env python


### TODO
# Allow include and exclude logic to use inequalities.
# This allows people to use more complex labels such as 1.356e19.
# Also, create $$ labels, which correspond to parameters that are linked
# AFTER the runs have been loaded $$self.ne[self.omp,self.sep]
# Maybe replace $$self by a lambda function of self.
# Together with the previous improvement one could include first to parse out
# a huge chunk of the database but then, once loaded, include and exclude
# again to really be able to use shot parameters.
# E.g.:
# include = {'dens': 'low'}
# Create family, loads all the cases labelled as low.
# Family.parse({'nesepm':'1e18<var<1e20'})
# Where var is the keyword for the quantity that will be obtained.
# For most databases, loading all the shots is not too much (usually 10-20).

# Also allow for experiments to accept 'all', as in load all the experiments.

# Common modules
import os
import sys
import types
import numpy as np
import pandas as pd
from collections import OrderedDict # as od
import itertools

# Special modules
from solpspy.factory import SolpsData

# Solpspy modules
from solpspy.tools.tools import SimpleNamespace, snv
from solpspy.tools.tools import priority, solps_property, read_config
from solpspy.tools.tools import parse_dataframe, module_path
from solpspy.tools.tools import yaml_loader, exp_cross_references, yaml_xrefs
from solpspy.tools.tools import update
from solpspy.tools.tools import DefaultOrderedDict


#from solpspy.external_modules.progress.bar import Bar
try:
    #Maybe include a local copy in ext_modules? License OK?
    from tqdm import tqdm
    _forced_no_progress_bar = False
except:
    _forced_no_progress_bar = True



# =============================================================================
# Logging setup
import logging
familylog = logging.getLogger('Family')
if not familylog.handlers: #Prevents from adding repeated streamhandlers
    formatter = logging.Formatter(
            '%(name)s -- %(levelname)s: %(message)s')
    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    familylog.addHandler(ch)
    familylog.propagate = False #Otherwise, root logger also prints
# =============================================================================


class Family(object):
    """
    Parameters
    ----------
    default_index: bool or str, optional
        If True, then look for default index in root_path_to_index.
        If path, then look for index at that path.
        if False, then no default index is used.
        Default: False.
    
    include: list(dicts), optional
        List of including criteria. Each criterion works as an
        'and' statement, thus only the cases that specifically suffice
        at least one criterion completely are included.
        Default: [].
    
    exclude: list(dicts), optional
        List of excluding criteria. Each criterion works as an
        'and' statement, thus only the cases that specifically suffice
        at least one criterion completely are excluded.
        Default: [].
    
    order: list(str, dict), optional
        From left to right, increasing level of grouping priority.
        Default: ['experiment'].
    
    autobuild: bool, optional
        If True, then database is automatically built during __init__
        phase.  Default: True.
    
    autoload: bool, optional
        If True, SOLPS runs are automatically loaded during __init__
        phase.  Default: True.
    
    progress_bar: bool, optional
        If True, a progress bar will be shown during the (possibly long) loops.
        Default: True.
    

    Notes
    -----

    Rework the idea of default index.

    order should be either a list or an ordered dict.
    Maybe a list of dicts too? ['experiment', {'ip':'descending'}, 'pow']
    
    """
    def __init__(self, **kwargs):
        self.log = familylog
        self.config = read_config()['Family']
        self.module_path = module_path()
        self.samples = SimpleNamespace()
        self.built = False
        self.loaded = False


        default_index = priority('default_index', kwargs,
                self.config['default_index'])
        index = priority('index', kwargs, None)
        db = priority(['database','db','dbfile'], kwargs, None) #NOT YET USED
        self.include = priority('include', kwargs, self.config['no_include'])
        self.exclude = priority('exclude', kwargs, self.config['no_exclude'])
        self._order   = priority('order',   kwargs, self.config['no_order'])
        self._order = self.parse_order(self._order, default=True)
        autobuild = priority('autobuild', kwargs, self.config['autobuild'])
        autoload  = priority('autoload',  kwargs, self.config['autoload'])
        if _forced_no_progress_bar:
            self.progress_bar = False
        else:
            self.progress_bar = priority(['progress_bar', 'bar'], kwargs,
                    self.config['progress_bar'])

        ## A list, even if of just one element, is needed down the line
        if isinstance(self.include, dict):
            self.include = [self.include]
        if isinstance(self.exclude, dict):
            self.exclude = [self.exclude]


        self.path_index = []
        if index:
            self.add_index(index)
        if default_index is True:
            self.path_index.append(os.path.join(
                module_path(),self.config['root_path_to_index']))


        #Allow to accept a kwargs['database'] (list(db) or db) and deal
        #with it as if it were the main (only?) "index" for avail_experiments.
        # Also allow to pass a .yaml file with the database itself.
        self.avail_experiments = self._avail_experiments()
        
        if autobuild:
            self.build_db()
            self.experiments = []
            for exp in list(self.db['experiment']):
                if exp not in self.experiments:
                    self.experiments.append(exp)
            ### ATTENTION!!
            ##Self.order is already calling impose order
            #self._impose_order(self.order)

        if autobuild is not False and autoload:
            self.load_runs()

        self._impose_order()







    @property
    def order(self):
        """list(str, dict or OrderedDict: Order for the entries
        of the database.
        When dict, the value is True for ascending order and
        False for descending order.
        Lists will default to ascending (True).
        """
        return self._order
    @order.setter
    def order(self, od):
        if isinstance(od, list) or isinstance(od, OrderedDict):
            self._order = self.parse_order(od, default=True)
            self._impose_order(self._order)
        else:
            self.log.error(
                "Order must be eiter list(str, dict or OrderedDict.")
            raise TypeError
    #@order.setter
    #def order(self, od):
    #    if isinstance(od, list):
    #        self._order = od
    #        self.impose_order()
    #    elif isinstance(od, str):
    #        self._order = [od]
    #        self.impose_order()
    #    else:
    #        self.log.error(
    #            "Order must be eiter list(str, dict or OrderedDict.")
    #        raise TypeError





    def add_index(self, path):
        """Creates abspath to index, either form relative or
        abs path in config
        """
        self.path_index = list(itertools.chain(self.path_index, [path]))

    def rm_index(self,ind):
        if type(ind) is int:
            ind = [ind]
        elif type(ind) is not list:
            self.log.error("Ind must be either int or list(int)")
            return
        tmp = []
        for i, cond in enumerate(self.path_index):
            if i not in ind:
                tmp.append(cond)
        self.path_index = tmp



    def add_include(self, inc):
        """
        Parameters
        ----------
        inc : list(dict)
            New conditions to be added to self.include. Same format as
            include kwarg.

        Returns
        -------
        None
            The new criteria are automatically recalculated upon inclusion.
        """
        self.include = list(itertools.chain(self.include, [inc]))
        self._criteria()

    def rm_include(self, ind):
        """
        Parameters
        ----------
        ind : int or list(int)
            Indexes of self.include conditions to be removed.

        Returns
        -------
        None
            The new criteria are automatically recalculated upon removal.
        """
        if type(ind) is int:
            ind = [ind]
        elif type(ind) is not list:
            self.log.error("Ind must be either int or list(int)")
            return
        tmp = []
        for i, cond in enumerate(self.include):
            if i not in ind:
                tmp.append(cond)
        self.include = tmp
        self._criteria()


    def add_exclude(self, exc):
        """
        Parameters
        ----------
        exc : list(dict)
            New conditions to be added to self.exclude. Same format as
            exclude kwarg.

        Returns
        -------
        None
            The new criteria are automatically recalculated upon inclusion.
        """
        self.exclude = list(itertools.chain(self.exclude, [exc]))
        self._criteria()

    def rm_exclude(self, ind):
        """
        Parameters
        ----------
        ind : int or list(int)
            Indexes of self.exclude conditions to be removed.

        Returns
        -------
        None
            The new criteria are automatically recalculated upon removal.
        """
        if type(ind) is int:
            ind = [ind]
        elif type(ind) is not list:
            self.log.error("Ind must be either int or list(int)")
            return
        tmp = []
        for i, cond in enumerate(self.include):
            if i not in ind:
                tmp.append(cond)
        self.include = tmp
        self._criteria()




    def build_db(self):
        """ Ensambles the database using all the available databases, filtered
        by the include and the exclude criteria.

        It uses the available databases in databases/index.yaml or given index
        according to config/local.yaml preferences.

        Parameters
        ----------
        None
            Internal parameters self.include and self.exclude are used.

        Returns
        -------
        None
            self.db is created, containing the corresponding ordered database.
        """
        self._criteria()

        #fulldb is a list of ALL cases for each experiment name.
        #If include says {'experiment': 'all'} then all the names should
        # be used. I guess that this step should be done before expanding
        # include_all and exclude_all
        # Maybe instead adding experiment to the include all, this step
        #should be skipped and fulldb should be created without parsing.
        # and then experiment simply does not appear in include all, thus
        # it is not a matter of the parser, unless specifically stated.
        # For example include all, but exclude Exp1.
        # Exclude all with some extra parameters would remove those parameters.
        # from all the experiments.
        fulldb = []
        self._explist = []
        for crit in self._include_all:
            expname = crit['experiment']
            if expname not in self._explist:
                try:
                    self._explist.append(expname)
                    yaml_exp=yaml_loader(
                            self.avail_experiments[expname])[expname]
                    cases = yaml_exp['cases'][:]
                    try:
                        defaults = yaml_exp['defaults'].copy()
                    except:
                        defaults = {}
                    try:
                        description = yaml_exp['description']
                    except:
                        description = ''

                    for case in cases:
                        exp_dict = defaults.copy()
                        exp_dict.update({'experiment':expname})
                        exp_dict.update({'description': description})
                        exp_dict.update(case)
                        exp_dict['abspath'] = self._case_abspath(exp_dict)
                        exp_dict.update(yaml_xrefs(exp_dict))
                        case.update(exp_dict)

                    fulldb = list(
                            itertools.chain.from_iterable([fulldb, cases]))

                #ATTENTION: Improve exception selection..
                except:
                    self.log.exception(
                    "Experiment '{}' not found in catalogue.".format(expname))
        fulldb = pd.DataFrame(fulldb)


        df = pd.DataFrame()

        ### OLD: INTs remain ints
        ###Construct db with all the include_all criteria
        #for criterion in self._include_all:
        #    tmp = parse_dataframe(fulldb, criterion)
        #    df = df.append(tmp, ignore_index=True)

        for criterion in self._include_all:
            tmp = parse_dataframe(fulldb, criterion)
            for _, new_entry in tmp.iterrows():
                inc = True
                for _, entry in df.iterrows():
                    if entry.equals(new_entry):
                        inc = False
                        break
                if inc:
                    df = df.append(new_entry, ignore_index=True)

        #Substract all the exclude_all criteria
        #import pdb; pdb.set_trace()
        for criterion in self._exclude_all:
            ## Parsing {} results in all. Maybe not needed if []
            if criterion != {}:
                tmp = parse_dataframe(df, criterion)
                df = df.drop(tmp.index.values)
                df = df.reset_index(drop=True)
        #self.db = df.reset_index(drop=True)
        self.db = df
        self.built = True

        return


    def _avail_experiments(self):
        """ Gather all available experiments from databases in either
        json or yaml formats.

        Parameters
        ----------
        None

        Returns
        -------
        OrderedDict
            Contains experiments as keys and paths to corresponding json/yaml
            files.

        Notes
        -----
            The intended use consist in allowing to choose a version of the same
            experiment over some other version in a different database.
            In that sense, {'my-experiment': '/path/to/my/database.yaml'} will
            provide the source to read 'my-experiment'.

            It can also be used to check the available experiments in a dynamic
            session, as 'my-experiment' in self.avail_experiments is bool.
        """
        rpath = self.path_index[:]
        rpath.reverse()
        # ATTENTION: This still did not make the order of exp correct.
        #experiments = {}
        experiments = OrderedDict()
        for index in rpath:
            try:
                rlist = yaml_loader(index)[:]
                #rlist.reverse() #Keep same order: Top = highest priority
                folderindexpath = index.rstrip(os.path.basename(index))
            except IOError:
                self.log.exception(
                    "Index '{}' could not be opened.".format(index))
            except Exception:
                self.log.exception(
                        "Index '{}' could not be found/read".format(index))

            for db in rlist:
                if type(db) is dict:
                    dbpath = os.path.join(folderindexpath,list(db.keys())[0])
                    if not os.path.exists(dbpath):
                        self.log.error(
                                "DB file '{}' could not be found/read".format(
                                    list(db.keys())[0]))
                        continue
                    dummy = list(db.values())[0]
                    for exp in dummy:
                        experiments.update({exp:dbpath})
                if type(db) is str:
                    dbpath = os.path.join(folderindexpath,db)
                    try:
                        dbcontent = yaml_loader(dbpath)
                    except:
                        self.log.error(
                            "DB file '{}' could not be found/read".format(db))
                        continue
                    for exp in list(dbcontent.keys()):
                        experiments.update({exp:dbpath})
        return experiments








    def parse_db(self, dicty):
        """ Alias for parse_dataframe which already provides self.db as
        the corresponding database.

        Parameters
        ----------
        dicty : dict
            Dict to be used as filter to parse self.db

        Returns
        -------
        pd.DataFrame
            Contains all the occurrences meeting the filter criteria.
        """
        return parse_dataframe(self.db,dicty)




    @staticmethod
    def parse_order(order, default=None):
        """
        Parameters
        ----------
        order: list(str, dict) or OrderedDict
            It indicates the labels to order and also how to order them,
            e.g., ['experiment',{'ip':False},'fhecore']
            Internal OrderedDicts may contain more than one key, but
            normal dicts should only contain a single  key or order might
            be random in some Python versions.

        Returns
        -------
        OrderedDict
            The str will be expanded to key:None entries.
            
        """
        parsed = OrderedDict()
        if isinstance(order,list):
            for ele in order:
                if isinstance(ele, str):
                    parsed[ele] = default
                elif isinstance(ele, dict):
                    parsed.update(ele)
            return parsed
        elif isinstance(order, OrderedDict):
            return order
        else:
            self.log.error("Order must be list(str, dict) or OrderedDict")
            raise TypeError

#    ## ATTENTION 2021: Read Note for public impose_order
#    ## ATTENTION 2021: Read Note for list(str) for str parameters
#    def _impose_order(self, order=None, replace=False):
#        """ Use list to order columns of db, creating a boxing effect.
#
#        Parameters
#        ----------
#        order : list(str), optional.
#            List to be used as ordering criterion.
#            If order is None, then self.order is used.
#            If order is not None, then order is used and self.order
#            is updated.
#
#
#        Notes
#        -----
#
#        Create a new impose_order to be used publicly.
#        It should not mutate self, but return a df object which
#        is a copy. Then, it can be used by users who want to temporarily
#        change the order
#
#        Then reassign _impose_order to call that function and update
#        self.db, self.runs and self.info.
#        However, that would create an extra object even if temporarily.
#        Is that too much?? Most likely not??
#        But wait until later, when tests have been created
#
#        Additionally, str parameters should be allowed to pass a list of
#        str as their value, so that str values are ordered according
#        to that lits intead of alphabetical order (or whatever is done
#        internally)
#        This might be achiveable with creating categories, e.g. exp rank,
#        automatically for any column str and assigning the rank value
#        based on either position (if nothing is given) or on given list.
#        In fact, this could be done for ANYTHING.
#        And it could, in fact solve the problem of experiment ordering.
#        
#        """
#        if order is None:
#            order = self.order.copy()
#        else:
#            order = self.parse_order(order, default=True)
#
#        #For experiment, create a temporary column with numerical indexes.
#        for i, exp in enumerate(self.experiments):
#            self.db.loc[self.db['experiment'] == exp, 'exp rank'] = i
#
#
#        #Substitute experiment by exp rank in order
#        try:
#            if isinstance(order, list):
#                order[order.index('experiment')] = 'exp rank'
#            elif isinstance(order, OrderedDict):
#                order = OrderedDict( [('exp rank', v) if k == 'experiment'
#                    else (k, v) for k, v in order.items()])
#        except:
#            pass
#
#        # Order database according to order
#        try:
#            #self.db = self.db.sort_values(by=order)
#            self.db = self.db.sort_values(
#                    by=list(order.keys()), ascending=list(order.values()))
#        except KeyError:
#            self.log.error("Ordering key not present as database labels")
#        del self.db['exp rank']
#
#        #Ordering index. Will help to re-order mds list
#        self._last_odin = np.array(self.db.index.astype(int))
#        self.db = self.db.reset_index(drop=True)
#        try:
#            self.runs = [self.runs[i] for i in self._last_odin]
#            self.info = [self.info[i] for i in self._last_odin]
#        except:
#            self.log.exception("No loaded runs, therefore ordering skipped.")

    def _impose_order(self, order=None, replace=False, cleanup=True):
        """ Use list to order columns of db, creating a boxing effect.

        Parameters
        ----------
        order : list(str), optional
            List to be used as ordering criterion.
            If order is None, then self.order is used.
            If order is not None, then order is used and self.order
            is updated.

        cleanup : bool, optional
            If True (default) then ranking columns are deleted.
            False maybe used for debugging.


        Notes
        -----

        Create a new impose_order to be used publicly.
        It should not mutate self, but return a df object which
        is a copy. Then, it can be used by users who want to temporarily
        change the order

        Then reassign _impose_order to call that function and update
        self.db, self.runs and self.info.
        However, that would create an extra object even if temporarily.
        Is that too much?? Most likely not??
        But wait until later, when tests have been created

        
        """
        if order is None:
            order = self.order.copy()
        else:
            order = self.parse_order(order, default=True)

        # Create ranking for each parameter for which list was given.
        ranked = []
        for param, od_param in order.items():
            if isinstance(od_param, list):
                placeholder = param+' rank'
                ranked.append(param)
                for i, itm in enumerate(od_param):
                    self.db.loc[self.db[param] == itm,
                            placeholder] = od_param.index(itm)
            #Experiment is special,  by default order is input order.
            elif param == 'experiment':
                ranked.append('experiment')
                for i, exp in enumerate(self.experiments):
                    self.db.loc[self.db['experiment'] == exp,
                        'experiment rank'] = i

        #Substitute experiment by exp rank in order
        try:
            for param in ranked:
                if param == 'ip':
                    import pdb; pdb.set_trace()
                if isinstance(order, list):
                    order[order.index(param)] = param+' rank'
                elif isinstance(order, OrderedDict):
                    order = OrderedDict([
                        (param+' rank', True) if k in ranked else (k, v)
                        for k, v in order.items()])
        except:
            pass

        # Order database according to order
        try:
            #self.db = self.db.sort_values(by=order)
            self.db = self.db.sort_values(
                    by=list(order.keys()), ascending=list(order.values()))
        except KeyError:
            self.log.error("Ordering key not present as database labels")

        # Clean up auxiliary ranking columns.
        if cleanup:
            for param in ranked:
                del self.db[param+' rank']

        #Ordering index. Will help to re-order other elements.
        self._last_odin = np.array(self.db.index.astype(int))
        self.db = self.db.reset_index(drop=True)

        # Reassign previously created lists and arrays with new order.
        self.experiments = list(self.db['experiment'].unique())
        try:
            self.runs = [self.runs[i] for i in self._last_odin]
            self.info = [self.info[i] for i in self._last_odin]
        except:
            self.log.exception("No loaded runs, therefore ordering skipped.")





    def load_runs(self, **kwargs):
        """ Ensambles a list with mds and rundir shots in self.db.
        self.db is created automatically.
        After loading, evaluates all the !Lambda parameters.
        The self.info and the self.runs[:].info is updated too.

        Parameters
        ----------
        load_by: str, optional.
            Label of the parameter to be used as input for SolpsData.
            Default: 'id'



        Returns
        -------
        None
            self.runs and self.info are created and attached directly
            to self. Also, individual runs have now a .info with the labels.

        Notes
        -----
        In the future, accept various load_by so that they are consecutively
        tried after failure.

        Maybe think about including this in the future instead:
        load_as: str, optional.
            Load the solps simulation as 'mds' or 'rundir', using
            nshot or abspath, respectively.
            Default: Loaded using 'id'

        """
        self.runs = []
        self.info = []

        load_by = priority(['by', 'load_by'], kwargs, self.config['load_by'])

        progress_bar = priority(
                ['progress_bar','bar'], kwargs, self.progress_bar)

        if progress_bar and not _forced_no_progress_bar:
            rowgen = tqdm(self.db.iterrows(), total=self.db.shape[0],
                    desc='Loading', unit='runs')
        else:
            rowgen = self.db.iterrows()

        try:
            for i,row in rowgen:
                lb = row[load_by]
                if isinstance(lb, float) or isinstance(lb,int):
                    lb = int(lb)
                sim = SolpsData(lb)
                ## Evaluate all the entries in row which are functions.
                ## They are assumed to be functions of sim(SolpsData).
                for column, itm in row.iteritems():
                    if isinstance(itm, types.FunctionType):
                        row[column] = itm(sim) #Not sure if mutation, but...
                        self.db.at[i, column] = itm(sim)
                sim.info = row
                self.runs.append(sim)
                self.info.append(row)

            self.loaded = True

        except:
            self.log.error("Loading of DB could not be completed")
            self.loaded = False



    
        ## Atempt in case we do not mind some beign None, but it is probably better
        ## to fail and know it.
        #for i,row in rowgen:
        #    try:
        #        lb = row[load_by]
        #        if isinstance(lb, float) or isinstance(lb,int):
        #            lb = int(lb)
        #        #tmp = SolpsData(row[load_by])
        #        tmp = SolpsData(lb)
        #        tmp.info = row
        #        self.runs.append(tmp)
        #        self.info.append(row)
        #    except:
        #        self.log.error("Load_by {} did not yield a correct SolpsData instance.".format(lb))
        #        self.runs.append(None)
        #        self.info.append("Not loaded")
        #self.loaded = True




    def sample(self, func, args=[], kwargs={}, name=None):
        """ Samples each run of the database in order given a function.

        Parameters
        ----------

        func: function or lambda function
            It must accept a SolpsData object as its parameter and can accept
            a list of additional positional arguments and a dictionary of
            optional key arguments.

        args: list, optional
            List of additional positional arguments for func.
            The SolpsData MUST NOT be included in this list, since it is always
            the default first argument.
            Default: []

        kargs: dict, optional
            List of optional key arguments of func.
            Default: {}


        name: str, optional
            If name is given, then sample is saved in self.samples with that
            name instead of being returned.
            Default: None.

        Returns
        -------
        (n entries, dims)

            If name is None, then returns a numpy array of dimensions:
            (number of database entries, dims) where dims is the dimensions
            of the returned variable of func.
            If name is not None, then that numpy array is attached to
            self.samples under the given name.

        """
        if not callable(func):
            self.log.error("Func must be callable.")
            raise TypeError

        if not hasattr(self,'runs'):
            self.log.info("No runs were loaded, thus loading them now.")
            self.load_runs()

        tmp= []
        if self.progress_bar:
            enu = tqdm(enumerate(self.runs), total = len(self.runs),
                    desc='Sampling', unit='samples')
        else:
            enu = enumerate(self.runs)
        for i,run in enu:
            try:
                #tmp.append(func(run))
                tmp.append(func(run, *args, **kwargs))
            except:
                self.log.exception(
                "Function could not be correctly applied to runs[{}]".format(i))
                tmp.append(np.nan)
        tmp = np.array(tmp)


        if name and isinstance(name,str):
            setattr(self.samples, name, tmp)
        else:
            return tmp


#    def iterslice(self, func):
#        """
#        Generator
#
#        Allow to accept order as an argument to overwrite temporarily the
#        default order, self.order.
#        """
#        if not callable(func):
#            self.log.error("Func must be callable with run as parameter.")
#            raise TypeError
#
#
#        labels = self.order[:-1]
#        basis =  self.order[-1]
#
#
#
#        df = self.db.copy()
#        # JUST FOR CHECKING
#        tmp = lambda x: os.path.join(x['basepath'], x['path'])
#        check = df[['basepath','path']].apply(tmp,axis=1)
#        embed()
#        # JUST FOR CHECKING
#
#
#        #for label in labels:
#            #Get unique values for each label
#
#
#        #for run in tmp_list:
#        #    slice.append(func(run))
#




    def iterdb(self):
        """
        Generator. NON GENERAL CASE.

        Allow to accept order as an argument to overwrite temporarily the
        default order, self.order.
        """
        try:
            for index, row in self.db.iterrows():
                yield self.runs[index], dict(row)
        except:
            for index, row in self.db.iterrows():
                yield None, dict(row)


    def unique(self,label):
        return self.df_unique(self.db, label)

    @staticmethod
    def df_unique(df, label):
        """
        Maybe improve logic of what is consider 'unique' (0.59999 vs 0.6, etc)
        Also, move to tools for a more general use?
        self.unique is already replacing it anyway.
        """
        unique = []
        for index,row in df.iterrows():
            if row[label] not in unique:
                unique.append(row[label])
        return unique




    ## ATTENTION: IT ONLY WORKS FOR TWO QUANTITIES IN ORDER
    #@profile
    def iterslice(self, func, order=None):
        """
        Generator

        Allow to accept order as an argument to overwrite temporarily the
        default order, self.order.

        Yields 
        ------
        x, y, dict
            x: deep-most ordering parameter or basis.

            y: value of func.

            dict: self.info, labels (ordering hierarchy), and prettify.

        Notes
        -----
        For the future, allow to call and alraedy created sample to save time,
        using info to gather the relevant information on each interation.

        ATTENTION: Users should be allowed to use real sim data as ordering
        since this is done after loading.

        """
        self.log.warning("This function is still in a building phase.")

        if not hasattr(self,'runs'):
            self.log.info("No runs were loaded, thus loading them now.")
            self.load_runs()

        df = self.db.copy()
        if not callable(func):
            self.log.error("Func must be callable.")
            raise TypeError

        if not order:
            order = self.order

        labels = order[:-1]
        basis =  order[-1]

        ## Create labels and basis out of order
        #if isinstance(order,list):
        #    labels = []
        #    for label in order[:-1]:
        #        try:
        #            label.append(label.key)
        #            label.append(label)

        #    #labels = 
        #    basis =  order[-1]
        #elif isinstance(order, OrderedDict):
        #    labels = order.keys()[:-1]
        #    basis =  order.keys()[-1]
            

        rdict = {}
        if len(labels) == 2:
            if self.progress_bar:
                genu = tqdm(df[labels[0]].unique(),
                    total = len(df[labels[0]].unique()),
                    desc='Iterating', unit='iterations')
            else:
                genu = df[labels[0]].unique()

            #genu = df[labels[0]].unique()

            #ATTENTION: UNIQUE MIGHT NOT NECESSARILY MEAN IN ORDER.
            for val1 in genu:
                rdict[labels[0]] = val1
                if isinstance(val1, float):
                    rdf = df[np.isclose(df[labels[0]], val1)]
                elif isinstance(val1, int):
                    rdf = df[np.isclose(df[labels[0]], val1)]
                elif isinstance(val1, str):
                    rdf = df[(df[labels[0]] == val1)]

                for val2 in rdf[labels[1]].unique():
                    rdict[labels[1]] = val2
                    if isinstance(val1, float):
                        rrdf = rdf[np.isclose(rdf[labels[1]], val2)]
                    elif isinstance(val1, int):
                        rrdf = rdf[np.isclose(rdf[labels[1]], val2)]
                    elif isinstance(val1, str):
                        rrdf = rdf[(rdf[labels[1]] == val2)]

                    indices = rrdf.index
                    basis_uniques = rrdf[basis].unique()
                    variable = np.full(len(indices), np.nan)
                    x = np.full(len(indices), np.nan)
                    mimdic = {}
                    for i, ind in enumerate(indices):
                        variable[i] = (func(self.runs[ind]))
                        x[i] = basis_uniques[i]
                        mimdic = self.minimum_dict(dict(df.iloc[ind]),mimdic)

                    mimdic['basis'] = basis
                    mimdic['labels'] = labels
                    yield x, variable, mimdic

        elif len(labels) == 1:
            for val1 in df[labels[0]].unique():
                rdict[labels[0]] = val1
                if isinstance(val1, float):
                    rdf = df[np.isclose(df[labels[0]], val1)]
                elif isinstance(val1, int):
                    rdf = df[np.isclose(df[labels[0]], val1)]
                elif isinstance(val1, str):
                    rdf = df[(df[labels[0]] == val1)]

                indices = rdf.index
                basis_uniques = rdf[basis].unique()
                variable = np.full(len(indices), np.nan)
                x = np.full(len(indices), np.nan)
                mimdic = {}
                for i, ind in enumerate(indices):
                    variable[i] = (func(self.runs[ind]))
                    x[i] = basis_uniques[i]
                    mimdic = self.minimum_dict(dict(df.iloc[ind]),mimdic)

                mimdic['basis'] = basis
                mimdic['labels'] = labels
                yield x, variable, mimdic






    @staticmethod
    def minimum_dict(dict1, dict2):
        if dict1 == {}:
            return dict2
        if dict2 == {}:
            return dict1

        mdic = {}
        for k1,v1 in list(dict1.items()):
            try:
                if ((isinstance(v1,float) or isinstance(v1,int))
                    and np.isclose(v1,dict2[k1])):
                    mdic[k1] = v1
                elif isinstance(v1,str) and (v1 == dict2[k1]):
                    mdic[k1] = v1
            except:
                pass
        return mdic





    def plot_slice(self, func, **kwargs):
        pass

















    def _expand_criteria(self, criteria=None):
        """
        """
        if criteria is None:
            criteria = [{}]
        elif not isinstance(criteria, list):
            criteria = [criteria]

        criteria_all = []
        for criterion in criteria:
            criteria_all.append(self._expand_combinations(criterion))
        return list(itertools.chain.from_iterable(criteria_all))


    def filter(self,include=None,exclude=None):
        """ Filter takes self and returns a "family" proxy with only
        the required cases.
        Runs and info are the same objects as the original.
        If one wants copies, one has to reload the runs.
        """
        if include is None:
            include = [{}]
        elif not isinstance(include, list):
            include = [include]
        if exclude is None:
            exclude = [{}]
        elif not isinstance(exclude, list):
            exclude = [exclude]


        fam = Family(default_index=False, index=None, db = None,
                include=include, exclude=exclude, order=self.order,
                autobuild=False, autload=False, bar=self.progress_bar)


        df = pd.DataFrame()

        includes = self._expand_criteria(include)
        excludes = self._expand_criteria(exclude)

        for criterion in includes:
            tmp = parse_dataframe(self.db, criterion)
            for _, new_entry in tmp.iterrows():
                inc = True
                for _, entry in df.iterrows():
                    if entry.equals(new_entry):
                        inc = False
                        break
                if inc:
                    #df = df.append(new_entry, ignore_index=True)
                    df = df.append(new_entry)


        ## Problem!! parse_dataframe does not work with index not starting on zero.
        ## For exclude, a reset in the index is required.
        ## But saving the included indices and mapping the droped by excluded,
        ## I can still know which ones are which.

        save_ind = df.index.to_list()
        df = df.reset_index(drop=True)

        #Substract all the exclude_all criteria
        for criterion in excludes:
            if criterion != {}:
                tmp = parse_dataframe(df, criterion)

                # Drop indices in positions tmp.index.values
                for ind in tmp.index.values:
                    del save_ind[ind]

                df = df.drop(tmp.index.values)
                df = df.reset_index(drop=True)
        fam.db = df

        if self.loaded:
            # If cases are loaded, then append runs and info from .index.to_list
            ## For that, the index cannot be reset until after this step has been taken.
            fam.runs = []
            fam.info = []
            for ind in save_ind:
                fam.runs.append(self.runs[ind])
                fam.info.append(self.info[ind])
            fam.saved_ind = save_ind

        return fam



































#    ## ATTENTION: IT ONLY WORKS FOR TWO QUANTITIES IN ORDER
#    #@profile
#    def iterslice(self, func):
#        """
#        Generator
#
#        Allow to accept order as an argument to overwrite temporarily the
#        default order, self.order.
#        """
#        if not hasattr(self,'runs'):
#            self.log.info("No runs were loaded, thus loading them now.")
#            self.load_runs()
#
#        df = self.db.copy()
#        if not callable(func):
#            self.log.error("Func must be callable with run as parameter.")
#            raise TypeError
#        labels = self.order[:-1]
#        basis =  self.order[-1]
#        stoplevel = len(labels)-1
#
#
#        #def tree(df, labels, basis, stoplevel, level):
#        #    for val1 in df[labels[0]].unique():
#        #        if isinstance(val1, float):
#        #            rdf = df[np.isclose(df[labels[0]], val1)]
#        #        elif isinstance(val1, int):
#        #            rdf = df[np.isclose(df[labels[0]], val1)]
#        #        elif isinstance(val1, str):
#        #            rdf = df[(df[labels[0]] == val1)]
#
#        #    if level == stoplevel:
#        #        indices = rdf.index
#        #        basis_uniques = rdf[basis].unique()
#        #        variable = np.full(len(indices), np.nan)
#        #        x = np.full(len(indices), np.nan)
#        #        mimdic = {}
#        #        for i, ind in enumerate(indices):
#        #            variable[i] = (func(self.runs[ind]))
#        #            x[i] = basis_uniques[i]
#        #            mimdic = self.minimum_dict(dict(df.iloc[ind]),mimdic)
#
#        #        yield x, variable, mimdic
#
#        #    else:
#        #        tree(rdf, labels, basis, stoplevel, level+1)
#
#        return self._tree(df, labels,basis, stoplevel, 0)
#
#    def _tree(self, df, labels, basis, stoplevel, level):
#        for val1 in df[labels[0]].unique():
#            if isinstance(val1, float):
#                rdf = df[np.isclose(df[labels[0]], val1)]
#            elif isinstance(val1, int):
#                rdf = df[np.isclose(df[labels[0]], val1)]
#            elif isinstance(val1, str):
#                rdf = df[(df[labels[0]] == val1)]
#
#        if level == stoplevel:
#            indices = rdf.index
#            basis_uniques = rdf[basis].unique()
#            variable = np.full(len(indices), np.nan)
#            x = np.full(len(indices), np.nan)
#            mimdic = {}
#            for i, ind in enumerate(indices):
#                variable[i] = (func(self.runs[ind]))
#                x[i] = basis_uniques[i]
#                mimdic = self.minimum_dict(dict(df.iloc[ind]),mimdic)
#
#            yield x, variable, mimdic
#
#        else:
#            self._tree(rdf, labels, basis, stoplevel, level+1)
#






















#    def iterslice(self,func):
#        return self._iterslice(self,func)
#
#    class _iterslice:
#        def __init__(self, mother, func):
#            self.log = mother.log
#            self.db = df = mother.db.copy()
#            if not callable(func):
#                self.log.error("Func must be callable with run as parameter.")
#                raise TypeError
#            else:
#                self.func = func
#
#            # JUST FOR CHECKING
#            tmp = lambda x: os.path.join(x['basepath'], x['path'])
#            self.check = df[['basepath','path']].apply(tmp,axis=1)
#            # JUST FOR CHECKING
#
#            self.labels = mother.order[:-1]
#            self.basis =  mother.order[-1]
#
#            #self.uniques = []
#            #for label in self.labels:
#            #    try:
#            #        self.uniques.append(df[label].unique())
#            #    except:
#            #        self.log.exception(
#            #            "'{}' not found in database.".format(label))
#
#
#
#
#            #self.tree = {}
#            self.stop = len(self.labels)-1
#            self.tree = self._tree(df,0)
#
#
#
#            embed()
#
#
#        def __iter__(self):
#            return self
#
#        #def __next__(self):
#        #    if len(self.labels) == 1:
#
#
#
#
#        def _tree(self, db, level):
#            """
#            Used for the non-general case
#            """
#            label = self.labels[level]
#            try:
#                uniques = db[label].unique()
#                tree = OrderedDict({str(unique): None for unique in uniques})
#            except:
#                self.log.exception(
#                    "'{}' not found in database.".format(label))
#
#            for value in uniques:
#                if isinstance(value, float):
#                    reduced_db = db[np.isclose(db[label], value)]
#                elif isinstance(value, int):
#                    reduced_db = db[np.isclose(db[label], value)]
#                else:
#                    reduced_db = db[(db[labels] == value)]
#
#                if level == self.stop:
#                    tree[str(value)] = db[self.basis].unique()
#
#                else:
#                    tree[str(value)] = self._tree(reduced_db, level+1)
#
#            return tree
#
##
##        def _tree(self, db, level):
##            label = self.labels[level]
##            try:
##                uniques = db[label].unique()
##            except:
##                self.log.exception(
##                    "'{}' not found in database.".format(label))
##
##            for value in uniques:
##                if isinstance(value, float):
##                    reduced_db = db[np.isclose(db[label], value)]
##                elif isinstance(value, int):
##                    reduced_db = db[np.isclose(db[label], value)]
##                else:
##                    reduced_db = db[(db[labels] == value)]
##
##                if level == self.stop:
##                    #tree[str(value)] = value
##                    #tree[str(value)] = db[self.basis].unique()
##                    for yld  in db[self.basis].iterrows():
##                        yield yld
##
##                else:
##                    self._tree(reduced_db, level+1)
##
##




    def set_style(self, style):
        """
        """
        if isinstance(style,list):
            pass
        elif isinstance(style,dict):
            pass





# =============== PRIVATE METHODS =============================================
    def _read_fulldb(self):
        """ Read all available databases, in either json or yaml format.
        """


    def _criteria(self):
        """ Creates a list containing the criteria to be used for the selection
        of the SOLPS simulations in the various databases.
        The final criteria are a mixture of the expanded include minus the
        expanded exclude.

        Parameters
        ----------
        None
            It uses self.include and self.exclude.

        Returns
        -------
        None
            It creates self._include_all and self._exclude_all, which contain the
            respective list with all the requested combinations given by
            self.include and self.exclude.

        Notes
        -----
        It needs to remove exact copies.
        """
        include_all = []
        for criterion in self.include:
            include_all.append(self._expand_combinations(criterion))
        self._include_all =  list(itertools.chain.from_iterable(include_all))
        exclude_all = []
        for criterion in self.exclude:
            exclude_all.append(self._expand_combinations(criterion))
        self._exclude_all =  list(itertools.chain.from_iterable(exclude_all))


    @staticmethod
    def _expand_combinations(dicty):
        """ Expands the dictionary by creating all valid combinations of
        the values of all the keys, which values can be lists.

        It takes a dict which values might or might not be list with multiple
        possibilities and gives back a list of "flattened" dicts, which keys
        only contain a single value, float, str, etc.

        Parameters
        ----------
        dicty : dict
            Dictionary to be expanded. The values for each key, which may be
            a list of values, will be combined.

        Returns
        -------
        list(dict)
            List containing all possible combinations with keys and values
            of dicty.
        """
        tmp = []
        ditems = list(dicty.items())
        dnames = [item[0] for item in ditems]
        for key, values in ditems:
            if type(values) != list:
                values = [values]
            tmp.append(values)
        combinations = list(itertools.product(*tmp))
        a = [dict(list(zip(dnames, values))) for values in combinations]
        return a



    @staticmethod
    def _shorter_str(tmp, ln):
        """ Return shortened string when it is longer than certain value.

        Parameters
        ----------
        tmp : str
            String to be tested and, if required, shortened.
        ln : int
            Number of characters above which the string tmp should be trimmed.

        Returns
        -------
        str
            It equals tmp, if it was within accepted lenght, or trimmed tmp.
        """
        tmp = str(tmp)
        ln = int(ln)
        if len(tmp) > ln:
            return '...'+tmp[-ln:]
        else:
            return tmp



    @staticmethod
    def _case_abspath(case):
        try:
            return case['abspath']
        except:
            pass

        try:
            rootpath = case['rootpath']
        except:
            rootpath = ''

        try:
            basepath = case['basepath']
        except:
            basepath = ''

        try:
            path = case['path']
        except:
            path = ''

        return os.path.join(rootpath,basepath,path)



    #def __repr__(self):
    def __str__(self): #Just doing self does not show the database, sadly.
        """
        Notes
        -----
            The return works.. somehow. I dont't want to impose order in
            id, though.
            Does it acknowledge it correctly?
            Return without ordering would be:
                return tmp.to_string()
        """
        tmp = self.db.copy()
        # Global column width, if any
        try:
            gwidth= int(self.config['repr']['global_column_width'])
            for col in list(tmp):
                try:
                    tmp[col] = tmp[col].apply(
                            lambda x: self._shorter_str(x, gwidth))
                except:
                    pass
        except:
            pass
        # Column width per column name
        if 'column_width' in self.config['repr']:
            for col, width in list(self.config['repr']['column_width'].items()):
                try:
                    tmp[col] = tmp[col].apply(
                            lambda x: self._shorter_str(x, width))
                except:
                    pass

        show = ['id']
        show = list(itertools.chain.from_iterable([show,self.order]))
        return tmp[show].to_string()
        #Create view with hierarchical indices order[:-1

    def show(self):
        print(self)
        return










#@profile
def main():
    from IPython import embed
#    logging.getLogger().setLevel(logging.DEBUG)




##    include = [{'experiment': ['21303 divIIb ff', '21303 divIIb ff dome']},
##            {'experiment':['21303 divIIb ff vertical'], 'albedo':0.350, 'pin': 1.00}]
#    include = [{'experiment': '21303 divIIb ff'}]
#
#    #exclude = [{'experiment':'21303 divIIb ff vertical'},
#    #       {'experiment':'21303 divIIb ff dome', 'albedo':[0.15, 0.350], 'pin':0.70}]
#    #exclude = [{'pin':1.0}]
#    exclude = []
#
#    order   = ['experiment','pin','albedo']
#
#    data = Family(include=include, exclude=exclude, order=order)



#    #Testing with new, more real database.
    #include = [
    #        {'experiment': 'aug ddnu alternative geometry'}]
    #include = [
    #        {'experiment': 'aug ddnu alternative geometry'},
    #        {'experiment': '21303 divIIb ff'}]
    #include = [
    #        {'experiment': '21303 divIIb ff'},
    #        {'experiment': 'aug ddnu alternative geometry'}]
    #include = [
    #        {'experiment': '21303 divIIb ff'},
    #        {'experiment': 'aug'}]


    #include = [
    #        {'experiment': 'aug ddnu alternative geometry', 'ip':[0.6]},
    #        {'experiment': 'aug ddnu original geometry', 'ip':0.6}]



    #order = ['experiment','ip', 'necore', 'tecore', 'ticore', 'exb','dia']
    #order = ['experiment','ip', 'necore', 'tecore']
    #order = ['experiment']
    #order = ['ip', 'experiment', 'necore', 'tecore', 'ticore', 'exb','dia']
    #order = ['ip', 'experiment', 'necore', 'tecore', 'ticore', 'exb','dia',
    #        'inert','vispar','visper','i-n']
    #order = ['ip', 'experiment', 'necore', 'tecore', 'path']



#    include = [{'experiment': 'aug ddnu alternative geometry'}]
#    order = ['ip', 'necore', 'tecore']
#
#    data = Family(include=include, order=order, autoload=False)
#    #data = Family(include=include)


#
#    tmp = yaml_loader(data._avail_experiments['just testing'])['just testing']
#    #from solpspy.tools.tools import yaml_translator
#    tmp2 = exp_cross_references(tmp)
#
#    tmp3 = []
#    for i,case in enumerate(tmp['cases']):
#        tmp3.append(yaml_xrefs(case))
#
#
#    include = [{'experiment': 'just testing'}]
#    data2 = Family(include=include, order=order, autoload=False)
#
#





    #data.iterslice(func=lambda x: x.te[x.omp, x.sep])







    #df = data.db.copy()
    #tmp = lambda x: os.path.join(x['basepath'], x['path'])
    #check = df[['basepath','path']].apply(tmp,axis=1)
    #hind = df.set_index(order)





    ################
    #include = [{'experiment': 'aug ddnu alternative geometry'}]

    #include = [{'experiment': 'aug ddnu nepow alt'}]
    #order = ['ip', 'necore', 'tecore']
    #exclude= []

    include = [{'experiment': 'aug ddnu nepow alt', 'necore':[2.5e19]},
               {'experiment': 'aug ddnu nepow alt', 'necore':3.5e19}]
    #order = ['ip', 'tecore', 'necore']
    order = ['ip', 'necore', 'tecore']
    #exclude = [{'experiment': 'aug ddnu nepow alt', 'tecore':450}]
    exclude = []


    data = Family(include=include, order=order, exclude=exclude, autoload=False)
    #data.load_runs(progress_bar=False)
    data.load_runs(progress_bar=True)
    #data.sample(lambda x: np.average(x.out.te,0), name='tet')
    #data.sample(lambda x: x.out.ds, name='ds')


    #iteruns = data.iterdb()

    #a = data.iterslice(lambda x: np.average(x.out.te,0)[x.sep])
    a = data.iterslice(lambda x: x.te[0, x.sep])
    print((next(a)))
    embed()


    import matplotlib.pyplot as plt
    print("Beginning to plot")
    colors={'1.0':'k','0.8':'b','0.6':'r'}
    ls = {'250':'-','350':'--','450':':'}
    for x,y,style in a:
        plt.plot(x,y, color=colors[str(style['ip'])],
                ls=ls[str(style['tecore'])],
                label= str(style['ip'])+'   '+str(style['tecore']))
    embed()


if __name__ == '__main__':
    main()






