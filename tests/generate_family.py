import os
import numpy as np
import pytest
from solpspy import *
from solpspy.tools.tools import module_path


##Just for manual testing
def family(scenario):
    index = os.path.join(module_path(),'tests/mockup_family_files/databases/index.yaml')
    return Family(index=index, default_index=False, autoload=False, **scenario)


scenarios = [
    {'name': '001',
     'include': [{'experiment': ['just testing', 'good testing']}],
     'exclude': [],
     'order':['experiment', 'ip', 'fhecore'],
     'ids': 'normal include'},

    {'name': '002',
     'include': [{'experiment': ['good testing', 'just testing']}],
     'exclude': [],
     'order':['experiment', 'ip', 'fhecore'],
     'ids': 'reversed include'},

    {'name': '003',
     'include': [{'experiment': ['good testing', 'just testing']}],
     'exclude': [],
     'order':['experiment', 'fhecore', 'ip'],
     'ids': 'reversed order'},

    {'name': '004',
     'include': [{'experiment': ['good testing', 'just testing', 'refs testing']}],
     'exclude': [],
     'order':['experiment', 'fhecore', 'ip'],
     'ids': 'reversed order'},
    ]

def main():
    import pdb
    import matplotlib.pyplot as plt
    from IPython import embed
    import pandas as pd

    for scenario in scenarios:
        path = 'mockup_family_files/{}'.format(scenario['name'])
        if not os.path.exists(path):
            os.makedirs(path)
        data = family(scenario)
        data.db.to_pickle('{}/pickled_mocking_database'.format(path))
        df2 = pd.read_pickle('{}/pickled_mocking_database'.format(path))
        print "Testing scenario {0}: {1}".format(scenario['name'], str(data.db.equals(df2)))


        data.load_runs(progress_bar=False)
        np.save('{}/out.te.npy'.format(path), data.sample(lambda x: x.out.te))
        np.save('{}/te.npy'.format(path), data.sample(lambda x: x.te))
        np.save('{}/na.npy'.format(path), data.sample(lambda x: x.na))

        npyload = np.load('{}/out.te.npy'.format(path))

    #embed()
    return

if __name__ == '__main__':
    main()



