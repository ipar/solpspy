Using (ana)conda
================

.. note::

   This is the barebone structure of the page.
   The Internet probably has more and better information.

To initialize::

    conda init my-shell-type


To create a ``conda`` environment::

    conda create --name name-of-my-env

It will install all the default packages and use the default ``python``
version in your system (I think).

A list of ``python`` packages can be given on creation::

    conda create --name name-of-my-env python=3.8 numpy

and this will only install ``python3.8`` and ``numpy``.

To enter/activate your ``conda env``::

    conda activate name-of-my-env


To exit/deactivate your ``conda env``::

    conda deactivate


To eliminate your ``conda env``::

    conda env remove -n name-of-my-env
