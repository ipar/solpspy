import numpy as np

def b2xpfe(mother):
    """Not completed as *_ext are not being implemented."""


    nx = mother.nx
    ny = mother.ny
    ns = mother.ns
    fna = mother.fna.copy()
    fch = mother.fch.copy()
    vol = mother.vol.copy()
    import pdb; pdb.set_trace()
    try:
        rza = mother.rza.copy()
    except:
        rza = np.tile(mother.za, (nx,ny,1))

    leftix = mother.leftix.copy()
    leftiy = mother.leftiy.copy()

    bottomix = mother.bottomix.copy()
    bottomiy = mother.bottomiy.copy()

    fne = np.zeros((nx,ny,ns))

    for si in xrange(ns):
        for yi in xrange(ny):
            for xi in xrange(nx):
                # poloidal component
                if leftix[xi,yi] >= 0:
                    voll = vol[leftix[xi,yi], leftiy[xi,yi]]
                    rzal = rza[leftix[xi,yi], leftiy[xi,yi], si]
                    fne[xi,yi,0] += fna[xi,yi,0,si]*(
                            rza[xi,yi,si]*voll +rzal*vol[xi,yi])/(
                                voll + vol[xi,yi])

                else:
                    fne[xi,yi,0] = 0

                if bottomix[xi,yi] >= 0:
                    volb = vol[bottomix[xi,yi], bottomiy[xi,yi]]
                    rzab = rza[bottomix[xi,yi], bottomiy[xi,yi], si]
                    fne[xi,yi,1] += fna[xi,yi,1,si]*(
                            rza[xi,yi,si]*volb +rzab*vol[xi,yi])/(
                                volb + vol[xi,yi])

                else:
                    fne[xi,yi,1] = 0

    return fne
