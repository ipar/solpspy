import numpy as np

def b2ardr(ninp, nout):
    """
    *-----------------------------------------------------------------------
    *.documentation
    *
    *  1. purpose
    *
    *     B2ARDR computes and writes to an external file a table of atomic
    *     rate coefficients for b2. The coefficients are obtained using
    *     either the ADPAK or the STRAHL set of routines.
    *
    *
    *  3. description (see also routine b2cdca)
    *
    *     This routine prepares a table of atomic rate coefficients for the
    *     processes of ionisation, recombination, heat loss, and charge
    *     exchange. The table is in the form required for use by the b2
    *     code /1,2/. It is stored in the common block /b2cmrc/ and is
    *     written to unit nout(1) for transfer into a b2 calculation.
    *
    *     The rate coefficients are obtained using either the ADPAK package
    *     or the STRAHL package.
    *     The ADPAK package was developed by R. A. Hulse of PPL, Princeton,
    *     and is described in Refs. /3,4/. For further information, contact
    *     R. A. Hulse at Plasma Physics Laboratory, Princeton University,
    *     P. O. Box 451, Princeton NJ 08543 (hulse@pppl.gov).
    *     The STRAHL package was developed by K. Behringer of IPP Garching
    *     and JET (now at the University at Stuttgart).  It is described in
    *     Refs. /5,6/. For further information, contact K. Behringer at
    *     Institut fuer Plasmaforschung, Universitaet Stuttgart,
    *     Pfaffenwaldring 31, D-7000 Stuttgart 80, Fed. Rep. Germany
    *     (behringe@ipf.uni-stuttgart.de).  [Since moved back to IPP].
    *
    *     Selected rate coefficients for hydrogen will be re-evaluated
    *     using tabulated data obtained from D. K. Stotler of PPPL and based
    *     on a code developed originally by J. C. Weisheit /7,8/. For
    *     further information on these rate coefficients please contact
    *     D. K. Stotler at PPPL (postal address above or stotler@pppl.gov).
    *
    *     A smoothing procedure may be applied to the ionisation rate
    *     coefficients to soften their steep dependence on temperature at
    *     low temperatures. This smoothing is controlled by an input
    *     parameter.
    *
    *
    *  4. references
    *
    *     /1/ B. J. Braams, "Computational Studies in Tokamak Equilibrium
    *     and Transport", thesis, State University at Utrecht, June 1986.
    *     /2/ B. J. Braams, "A Multi-Fluid Code for Simulation of the Edge
    *     Plasma in Tokamaks", NET Report Nr 68, January 1987.
    *     /3/ R. A. Hulse, "Numerical Studies of Impurities in Fusion
    *     Plasmas", Nuclear Technol./Fusion, vol.3 (1983) pp.259-272.
    *     /4/ R. A. Hulse, !! (detailed description?).
    *     /5/ K. Behringer, "Description of the Impurity Transport Code
    *     STRAHL", JET Report R(87)08, 1987.
    *     /6/ K. Behringer, !! (journal article?).
    *     /7/ Jon C. Weisheit, "Recombination in Dense Plasmas", J. Phys. B
    *     (Atom. Molec. Phys.) 8 (1975) 2556.
    *     /8/ R. K. Janev, D. E. Post, W. D. Langer, K. Evans, D. B.
    *     Heifetz and J. C. Weisheit, "Survey of Atomic Processes in Edge
    *     Plasmas", J. Nuclear Mater. 121 (1984) 10-16.
    *
    *
    *  5. parameters (see also routine b2cdcv)
    *
    *     ninp - (0:2) integer array, input.
    *     ninp specifies the input unit numbers, as follows.
    *     ninp(0): formatted; provides species parameters.
    *     ninp(1): formatted; provides input data for STRAHL calculation.
    *     ninp(2): formatted; provides input data from Weisheit code.
    *     ninp(3): un*formatted; provides default physics parameters.
    *     ninp(4): un*formatted; provides the geometry.
    *
    *     nout - (0:1) integer array, input.
    *     nout specifies the output unit numbers, as follows.
    *     nout(0): formatted; provides printed summary of rate coefs.
    *     nout(1): un*formatted; provides the table of rate coefficients.
    *
    *
    *  8. further comments
    *
    *     The routine computes and writes out the logarithm of the atomic
    *     rate coefficients in the form rl=logu(r). The function logu is a
    *     log protected against underflow: if 0.lt.r then logu(r)=log(r),
    *     if r.lt.0 then logu(r) is undefined, and if r.eq.0 then
    *     logu(r) is a large negative number so that exp(logu(r)) will
    *     surely underflow.
    *
    *-----------------------------------------------------------------------
    *.documentation-internal
    *
    *     The following common block has its outermost declaration in
    *     this routine; they need not be preserved between calls.
    *
    *     /b2cmrc/ contains a table of atomic rate coefficients.
    *      rtnt - integer constant.
    *     rtnt specifies the number of intervals in the discretisation
    *     of the temperature range. The table will contain data for
    *     (rtnt+1) values of the temperature variable.
    *      rtnn - integer constant.
    *     rtnn specifies the number of intervals in the discretisation
    *     of the density range. The table will contain data for (rtnn+1)
    *     values of the density variable.
    *      rtns - integer.
    *     rtns specifies the total number of atomic species; i.e.
    *       (sum inuc : 0.le.inuc.lt.nnuc : izhi(inuc)-izlo(inuc)+1) ,
    *     where inuc is a nuclear species index, izlo(inuc) is the lowest
    *     and izhi(inuc) is the highest atomic charge considered in b2 for
    *     that nuclear species.
    *     rtns equals the variable ns used elsewhere in b2.
    *      rtdumm - integer.
    *     rtdumm is a dummy integer variable, introduced in order to obtain
    *     an even-length sequence of integer variables in the common block.
    *      rtzmin, rtzmax, rtzn - (0:rtns-1) real*8 array.
    *     For 0.le.is.lt.rtns, rtzmin(is) and rtzmax(is) specify the atomic 
    *     charge range, and rtzn(is) specifies the nuclear charge
    *     of atomic species (is). 
    *     The izhi(inuc)-izlo(inuc)+1 charge states for each nuclear
    *     species are taken in natural order, starting at izlo(inuc); the
    *     highest state of nuclear species (inuc) is followed by the lowest
    *     state of (inuc+1). rtzmin, rtzmax and rtzn are equal to the arrays 
    *     zamin, zamax and zn used elsewhere in b2.
    *      rtt - (0:rtnt) real*8 array.
    *     rtt(0:rtnt) specifies the sequence of values of the temperature
    *     variable, expressed in ev, for which the rate coefficients are
    *     evaluated. rtt(0:rtnt) will be positive and increasing.
    *      rtn - (0:rtnn) real*8 array.
    *     rtn(0:rtnn) specifies the sequence of values of the density
    *     variable, expressed in m**-3, for which the rate coefficients
    *     are evaluated. rtn(0:rtnn) will be positive and increasing.
    *      rtlt - (0:rtnt) real*8 array.
    *     rtlt(0:rtnt) holds log(rtt(0:rtnt)).
    *      rtln - (0:rtnn) real*8 array.
    *     rtln(0:rtnn) holds log(rtn(0:rtnn)).
    *      rtlsa - (0:rtnt,0:rtnn,0:rtns-1) real*8 array.
    *     For (i,j,k) in (0:rtnt,0:rtnn,0:rtns-1), rtlsa(i,j,k) specifies
    *     logu(rtrsa), where rtrsa is the ionisation rate coefficient at
    *     electron temperature rtt(i), electron density rtn(j), for the
    *     process k->k+1. rtrsa has dimension m**3/s. The function logu is
    *     the same as (log) except that logu(0.0) is equal to some huge
    *     negative number.
    *      rtlra - (0:rtnt,0:rtnn,0:rtns-1) real*8 array.
    *     For (i,j,k) in (0:rtnt,0:rtnn,0:rtns-1), rtlra(i,j,k) specifies
    *     logu(rtrra), where rtrra is the recombination rate coefficient at
    *     electron temperature rtt(i) and electron density rtn(j), for the
    *     process k->k-1. rtrra has dimension m**3/s.
    *      rtlqa - (0:rtnt,0:rtnn,0:rtns-1) real*8 array.
    *     For (i,j,k) in (0:rtnt,0:rtnn,0:rtns-1), rtlqa(i,j,k) specifies
    *     logu(rtrqa), where rtrqa is the electron heat loss rate
    *     coefficient at electron temperature rtt(i) and electron density
    *     rtn(j), for processes starting from atomic species k. rtrqa has
    *     dimension eV*m**3/s.
    *      rtlcx - (0:rtnt,0:rtnn,0:rtns-1) real*8 array.
    *     For (i,j,k) in (0:rtnt,0:rtnn,0:rtns-1), rtlcx(i,j,k) specifies
    *     logu(rtrcx), where rtrcx is the charge exchange rate coefficient
    *     at neutral hydrogen temperature/amu rtt(i) and electron density
    *     rtn(j), for the process k->k-1. rtrcx has dimension m**3/s.
    *      rtlrd - (0:rtnt,0:rtnn,0:rtns-1) real*8 array.
    *     For (i,j,k) in (0:rtnt,0:rtnn,0:rtns-1), rtlrd(i,j,k) specifies
    *     logu(rtrrd), where rtrrd is the line radiation rate
    *     coefficient at electron temperature rtt(i) and electron density
    *     rtn(j), for processes starting from atomic species k. rtrrd has
    *     dimension eV*m**3/s.
    *      rtlbr - (0:rtnt,0:rtnn,0:rtns-1) real*8 array.
    *     For (i,j,k) in (0:rtnt,0:rtnn,0:rtns-1), rtlbr(i,j,k) specifies
    *     logu(rtrbr), where rtrbr is the bremsstrahlung radiation rate
    *     coefficient at electron temperature rtt(i) and electron density
    *     rtn(j), for processes starting from atomic species k. rtrbr has
    *     dimension eV*m**3/s.
    *      rtlza - (0:rtnt,0:rtnn,0:rtns-1) real*8 array.
    *     For (i,j,k) in (0:rtnt,0:rtnn,0:rtns-1), rtlza(i,j,k) specifies
    *     the effective charge state at
    *     electron temperature rtt(i), electron density rtn(j), for the
    *     superstage k. rtlza has dimension e. 
    *      rtlz2 - (0:rtnt,0:rtnn,0:rtns-1) real*8 array.
    *     For (i,j,k) in (0:rtnt,0:rtnn,0:rtns-1), rtlz2(i,j,k) specifies
    *     the effective squared charge at
    *     electron temperature rtt(i), electron density rtn(j), for the
    *     superstage k. rtlz2 has dimension e**2. 
    *      rtlpt - (0:rtnt,0:rtnn,0:rtns-1) real*8 array.
    *     For (i,j,k) in (0:rtnt,0:rtnn,0:rtns-1), rtlpt(i,j,k) specifies
    *     the cumulative ionisation potential at
    *     electron temperature rtt(i), electron density rtn(j), to reach
    *     superstage k. rtlpt has dimension eV. 
    *      rtlpi - (0:rtnt,0:rtnn,0:rtns-1) real*8 array.
    *     For (i,j,k) in (0:rtnt,0:rtnn,0:rtns-1), rtlpi(i,j,k) specifies
    *     the effective ionisation potential at
    *     electron temperature rtt(i), electron density rtn(j), to reach
    *     superstage k from superstage k-1. rtlpi has dimension eV. 
    *
    *     Description of some local variables:
    *
    *     nnucm - integer constant.
    *     nnucm is the maximum number of nuclear species.
    *
    *     nzmax - integer constant.
    *     nzmax is the maximum allowed value of the nuclear charge.
    *
    *     nnuc - integer.
    *     nnuc specifies the number of nuclear species.
    *
    *     nz - (0:nnucm-1) integer array.
    *     For inuc in (0:nnuc-1), nz(inuc) specifies the nuclear charge of
    *     the nuclear species inuc.
    *
    *     iyear - (0:nnucm-1) integer array.
    *     For inuc in (0:nnuc-1), iyear(inuc) specifies the ADAS year of
    *     the nuclear species inuc.
    *     (if -1 then ADAS year was not the same for all ADAS files)
    *
    *     rtyr - (0:ntns-1) integer array.
    *     Specifies the ADAS year of the atomic species.
    *     (if -1 then ADAS year was not the same for all ADAS files)
    *
    *     npart - (0:nnucm-1) integer array.
    *     Specifies the number of partitions of the homonuclear sequence.
    *
    *     izlo, izhi - (0:nnucm-1) integer array.
    *     For inuc in (0:nnuc-1), (izlo(inuc):izhi(inuc)) specifies the
    *     range of atomic charge state represented in b2 for the nuclear
    *     species inuc. It will hold that
    *       0.le.izlo(inuc).le.izhi(inuc)+1 and izhi(inuc).le.nz(inuc).
    *
    *     flag - character*8.
    *     flag specifies which code is to be used to calculate the rate
    *     coefficients. Its value may be 'adpak', 'strahl', 'adas' or 'amns'.
    *
    *     lblrc - character*120.
    *     lblrc identifies the instance of the program employed to prepare
    *     the table of atomic rate coefficients.
    *
    *     lnzero - real*8 constant.
    *     lnzero is a large negative real*8 number. It must be representable
    *     on every computer on which this code might run or might be
    *     compiled, but exp(lnzero) must underflow on every such computer.
    *
    *     tailep - real.
    *     tailep specifies a smoothing parameter for the temperature
    *     dependence of some rate coefficients. It must be non-negative and
    *     small compared to 1; tailep.eq.0.0 implies no smoothing.
    *     See the routine rattai for detailed description.
    *
    *     nidis, njdis - integer constant.
    *     nidis and njdis are dimension bounds for arrays idis and jdis.
    *
    *     idis - (0:nidis-1) integer array.
    *     idis is the selection of values of the temperature index at which
    *     output will be produced to display the density dependence of the
    *     rate coefficients.
    *
    *     jdis - (0:njdis-1) integer array.
    *     jdis is the selection of values of the density index at which
    *     output will be produced to display the temperature dependence of
    *     the rate coefficients.
    *
    *-----------------------------------------------------------------------
    """
