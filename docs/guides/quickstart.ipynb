{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "ad9c2531",
   "metadata": {},
   "source": [
    "# Quickstart"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a4d2e398",
   "metadata": {},
   "source": [
    "The source code of ``solpspy``: https://gitlab.com/ipar/solpspy"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "34f4f9c0",
   "metadata": {},
   "source": [
    "**Zen of ``solpspy``**:\n",
    "\n",
    "- Do not reinvent the wheel.\n",
    "\n",
    "- Standard is better than non-standard.\n",
    "\n",
    "- General is better than specific.\n",
    "\n",
    "- Specific is better than non-working.\n",
    "\n",
    "- Focus on the physics of the simulations, not on how to handle them.\n",
    "\n",
    "\n",
    "For that, `SolpsData` is a class that will take care of reading the files for you, so that you have fast and standardized access to the input and output data of any SOLPS run.\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "Disclaimer:\n",
    "\n",
    "> ``solpspy`` aims to ease the exploitation of SOLPS simulations by the general users and non-experts.  \n",
    "It **does not** intend to ease the usage of Python.\n",
    "\n",
    "I.e.: **Learn your Python** to realize the full potential of ``solpspy``.  \n",
    "If you do not have a solid grasp of the basic Python syntax, you won't be able to even know what is general Python and what is ``solpspy`` specific."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cd760739",
   "metadata": {},
   "source": [
    "## Quickstart with the SolpsData class"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "625d733e",
   "metadata": {},
   "source": [
    "The main object of ``solpspy`` is the `SolpsData` class.  \n",
    "To obtain a valid `SolpsData` instance, one needs either an MDS+ shot number or the path to SOLPS run directory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e9261774",
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import os\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "\n",
    "import solpspy\n",
    "\n",
    "#We will use one of the mockup test cases as an example.\n",
    "shot = solpspy.SolpsData(os.path.join(solpspy.module_path(),\n",
    "                        '../tests/rundir_src/119603/run'))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "35eed5e1",
   "metadata": {},
   "source": [
    "This shot object contains all the information that you may want out of your SOLPS simulation.  \n",
    "You can access different variables by just calling them:  \n",
    "> Variables in solpspy are, in general, lazy. Therefore, data are generally only loaded when they are needed, which is good for loading times and memory consumption."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b3f0e87a",
   "metadata": {},
   "outputs": [],
   "source": [
    "shot.te"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a1724c55",
   "metadata": {},
   "outputs": [],
   "source": [
    "shot.te.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a2fdb2b4",
   "metadata": {},
   "outputs": [],
   "source": [
    "shot.help(\"te\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "866f15ea",
   "metadata": {},
   "source": [
    "This shows that the property te is a (nx, ny) numpy array which contains the electron temperature in eV.\n",
    "Other quantities have other shapes, and one should always know what one is doing when manipulating indices, just like in any general numpy array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3a8ec223",
   "metadata": {},
   "outputs": [],
   "source": [
    "shot.te[shot.omp] # This is a radial profile at the outer midplane (omp)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a300566e",
   "metadata": {},
   "outputs": [],
   "source": [
    "shot.te[shot.omp].shape # It has a size of ny, as expected."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8f3c92c6",
   "metadata": {},
   "outputs": [],
   "source": [
    "shot.te[:, shot.sep] # This is a poloidal profile along the first flux surface in the SOL (sep)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e9df5154",
   "metadata": {},
   "outputs": [],
   "source": [
    "shot.te[:, shot.sep].shape  # It has a size of nx, as expected."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "511a73d6",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(shot.magnetic_geometry) # This case is an upper disconnected double-null configuration."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "71646e0c",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(shot.solps_code) # Version of SOLPS which figures on the header of files.\n",
    "print(shot.solpsversion) # Release version of the code on the header of files.\n",
    "#Annoyingy, solps_version would return solps-iter"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a14ea284",
   "metadata": {},
   "outputs": [],
   "source": [
    "shot.outmp.te.shape #This is the time trace of the electron temperature at the outer midplane (ntime, ny)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "75f20871",
   "metadata": {},
   "source": [
    "<br/>\n",
    "<br/>\n",
    "\n",
    "## Structure of the SolpsData class"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a8d5ff46",
   "metadata": {},
   "source": [
    "Indices (int):  \n",
    "- sep: Radial index of the first flux tube in the main SOL.\n",
    "- sep2: Radial index of the first flux tube in the secondary SOL, if it exists.\n",
    "- omp / imp: Poloidal index of the outer / inner midplane.\n",
    "- iout / iinn: Poloidal index of the main outer / inner target.\n",
    "- iout2 / iinn2: Poloidal index of the secondary outer / inner target, if it exists.\n",
    "\n",
    "Basic variables:\n",
    "- te / ti: electron / ion temperature.\n",
    "- ne: electron density.\n",
    "- na: density of each fluid species. This includes the fluid neutrals.\n",
    "- ua: parallel velocity of each fluid species. \n",
    "- dab2: EIRENE atomic density in the b2 grid.\n",
    "- dmb2: EIRENE molecular density in the b2grid.\n",
    "\n",
    "Time traces:\n",
    "- outmp / innmp: Name fields containing the time traces corresponding to the outer / inner midplanes.\n",
    "- out / nn: Same but for outer / inner main target.\n",
    "- out2 / inn2: Same but for outer / inner secondary target.\n",
    "\n",
    "Variables in time traces: The available quantities may differ depending on the type (midplane, target or region), but some are common: te, ti, ne, na, ..."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8c84db89",
   "metadata": {},
   "source": [
    "### Hierarchy of files"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a7bc9f9a",
   "metadata": {},
   "source": [
    "Some data are present in more than one file. For example, te could be read from b2fplasmf, b2fstate or b2fstati.  \n",
    "The specific hierarchy of files can be found and modified in the configuration files.  \n",
    "In general: b2fplasmf > b2fgmtry > b2fstate > b2fstati."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
