.. _api_reference:

API reference
=============

SolpsData objects
-----------------
When SolpsData is called, it will return one of the following objects
according to the passed parameters or its own inference methods.

LSN
^^^

:class:`classes.mds.MdsData`

:class:`classes.rundir.RundirData`


USN
^^^

:class:`classes.alt_mag_geom.MdsDataUSN`

:class:`classes.alt_mag_geom.RundirDataUSN`


DDNU
^^^^

:class:`classes.alt_mag_geom.MdsDataDDNU`

:class:`classes.alt_mag_geom.RundirDataDDNU`


CDN
^^^^
.. warning::

    Inference of SolpsData does not work with CDN.
    Therefore ``maggeom='cdn'`` must be used.


:class:`classes.alt_mag_geom.MdsDataCDN`

:class:`classes.alt_mag_geom.RundirDataCDN`
 


Plots
-----

:class:`plots.plot_class.VesselDataPlot`:
Plot the vessel.

:class:`plots.plot_class.GridDataPlot`:
Plot in the B2 physical mesh.

:class:`plots.plot_class.ComputationalDataPlot`:
Plot in the B2 computational mesh.

:class:`plots.plot_class.TriangDataPlot`:
Plot in the Eirene triangular physical mesh.


Masks
-----

:class:`classes.masks.MasksLSN`

:class:`classes.masks.MasksUSN`

:class:`classes.masks.MasksDDNU`



Other elements
--------------

:class:`classes.family.Family`:
To work with databases of SolpsData objects.


:class:`extensions.eq_output.EquationsOutput`:
Internal variables of the equations during iterations.
Only available for RundirData like objects.
The switch ``'b2wdat_iout' '4'`` must have been set for the run,
so that the ``output/`` directory is populated.

